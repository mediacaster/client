package com.taltenbach.mediacaster.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.client.ClientPlaylistController;
import com.taltenbach.mediacaster.model.Favorites;
import com.taltenbach.mediacaster.model.PlaylistEntry;
import com.taltenbach.mediacaster.util.ResultCallback;

import java.net.IDN;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 26/07/2018
 */

public class PlaylistFragment extends Fragment implements EnqueueMediaDialog.EnqueueMediaDialogObserver {
    public static final String TAG = "PlaylistFragment";

    private RecyclerView mPlaylistView;

	private View mRootView;
    private ClientPlaylistController mClientPlaylistController;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_playlist, container, false);

        final FloatingActionButton enqueueMediaButton = mRootView.findViewById(R.id.btn_enqueue_media);

        enqueueMediaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EnqueueMediaDialog dialog = new EnqueueMediaDialog();
                dialog.setTargetFragment(PlaylistFragment.this, 0);
                dialog.show(requireFragmentManager(), EnqueueMediaDialog.TAG);
            }
        });

        mPlaylistView = mRootView.findViewById(R.id.playlist_view);
        mPlaylistView.setLayoutManager(new LinearLayoutManager(getContext()));

        new ItemTouchHelper(new SwipeToDeleteCallback(getContext(), ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                if (mClientPlaylistController.canRemovePlaylistEntry(viewHolder.getAdapterPosition()))
                    return super.getSwipeDirs(recyclerView, viewHolder);

                return 0;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                removeEntry(viewHolder.getAdapterPosition());
            }
        }).attachToRecyclerView(mPlaylistView);

        mPlaylistView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0 && enqueueMediaButton.getVisibility() == View.VISIBLE)
                    enqueueMediaButton.hide();
                else if (dy < 0 && enqueueMediaButton.getVisibility() != View.VISIBLE)
                    enqueueMediaButton.show();
            }
        });

        /*if (savedInstanceState != null) {
            ClientFragment clientFragment = (ClientFragment) getParentFragment();

            if (clientFragment == null)
                throw new IllegalStateException("This fragment must have a ClientFragment as parent.");

            onConnected(clientFragment.getPlaylistController());
        }*/

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        ClientPlaylistController clientPlaylistController = getClientFragment().getPlaylistController();

        if (clientPlaylistController == mClientPlaylistController)
            return;

        mClientPlaylistController = clientPlaylistController;

        loadFavorites(new ResultCallback<Favorites>() {
            @Override
            public void onResult(Favorites favorites) {
                PlaylistAdapter playlistAdapter = new PlaylistAdapter(PlaylistFragment.this, mClientPlaylistController, favorites, mPlaylistView.getLayoutManager());
                mPlaylistView.setAdapter(playlistAdapter);
            }
        });
    }

    /*void onConnected(ClientPlaylistController playlistController) {
        mClientPlaylistController = playlistController;

        loadFavorites(new ResultCallback<Favorites>() {
            @Override
            public void onResult(Favorites favorites) {
                PlaylistAdapter playlistAdapter = new PlaylistAdapter(mClientPlaylistController, favorites, mPlaylistView.getLayoutManager());
                mPlaylistView.setAdapter(playlistAdapter);
            }
        });
    }*/

    void onDisconnected() {
        mClientPlaylistController = null;

        if (mPlaylistView.getAdapter() != null) {
            ((PlaylistAdapter) mPlaylistView.getAdapter()).destroy();
            mPlaylistView.setAdapter(null);
        }
    }
	
    @Override
    public void onDestroyView() {
        if (mPlaylistView.getAdapter() != null) {
            ((PlaylistAdapter) mPlaylistView.getAdapter()).destroy();
            mPlaylistView.setAdapter(null);
        }

        super.onDestroyView();
    }

    @Override
    public void onEnqueueRequested(String mediaUrl) {
        mClientPlaylistController.requestPlatformMediaEnqueue(mediaUrl);
    }
	
	private void loadFavorites(@Nullable ResultCallback<Favorites> resultCallback) {
        FavoriteViewModel favoritesViewModel;
        FragmentActivity activity = getActivity();

        if (activity == null)
            favoritesViewModel = ViewModelProviders.of(this).get(FavoriteViewModel.class);
        else
            favoritesViewModel = ViewModelProviders.of(activity).get(FavoriteViewModel.class);

        favoritesViewModel.loadFavorites(resultCallback);
    }
	
	void kickUser(String userName) {
        getClientFragment().kickUser(userName);
	}
	
	void banUser(String userName) {
		getClientFragment().banUser(userName);
	}
	
	void removeEntry(int pos) {
		final Snackbar snackbar = createDeletedEntrySnackbar();
		
		mClientPlaylistController.requestRemove(pos, new ClientPlaylistController.RemovedByServerCallback() {
				@Override
				public void onEntryRemovedByServer() {
					snackbar.dismiss();
				}
			});
				
		snackbar.show();
	}
	
	void removeEntry(@NonNull PlaylistEntry entry, int startSearchIndex) {
		final Snackbar snackbar = createDeletedEntrySnackbar();
		
		mClientPlaylistController.requestRemove(entry, startSearchIndex, new ClientPlaylistController.RemovedByServerCallback() {
				@Override
				public void onEntryRemovedByServer() {
					snackbar.dismiss();
				}
			});
			
		snackbar.show();
	}

    @NonNull
    private ClientFragment getClientFragment() {
        ClientFragment clientFragment = (ClientFragment) getParentFragment();

        if (clientFragment == null)
            throw new IllegalStateException("This fragment must have a ClientFragment as parent.");

        return clientFragment;
    }
	
	private Snackbar createDeletedEntrySnackbar() {
		return Snackbar.make(mRootView, R.string.deleted, Snackbar.LENGTH_LONG)
			.setAction(R.string.undo, new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					mClientPlaylistController.restoreRemoved();
				}
			})
			.addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
				@Override
				public void onDismissed(Snackbar transientBottomBar, int event) {
					if (event != DISMISS_EVENT_ACTION && event != DISMISS_EVENT_MANUAL)
						mClientPlaylistController.commitRemove();
				}
			});
	}

    public static PlaylistFragment newInstance() {
        return new PlaylistFragment();
    }
}
