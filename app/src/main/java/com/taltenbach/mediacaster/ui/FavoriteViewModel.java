package com.taltenbach.mediacaster.ui;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.taltenbach.mediacaster.model.Favorites;
import com.taltenbach.mediacaster.util.ResultCallback;

public class FavoriteViewModel extends AndroidViewModel {
    private Favorites mFavorites;

    public FavoriteViewModel(@NonNull Application application) {
        super(application);
    }

    public void loadFavorites(@Nullable final ResultCallback<Favorites> onLoadedCallback) {
        Favorites.load(getApplication(), new ResultCallback<Favorites>() {
            @Override
            public void onResult(Favorites favorites) {
                mFavorites = favorites;

                if (onLoadedCallback != null)
                    onLoadedCallback.onResult(favorites);
            }
        });
    }

    public Favorites getFavorites() {
        return mFavorites;
    }

    @Override
    protected void onCleared() {
        Favorites.close();
    }
}
