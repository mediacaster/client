package com.taltenbach.mediacaster.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.client.ClientPlaylistController;
import com.taltenbach.mediacaster.client.ConnectionFailedReason;
import com.taltenbach.mediacaster.client.ConnectionInfo;
import com.taltenbach.mediacaster.client.MediaPlayerController;
import com.taltenbach.mediacaster.client.PlaylistClientService;
import com.taltenbach.mediacaster.util.PlaylistServiceRunningChecker;

public class ClientFragment extends Fragment implements ConnectToServerDialog.ConnectToServerDialogObserver {
    public static final int TITLE_RES_ID = R.string.playlist_fragment_title;

    private static final int RECONNECTION_DELAY = 5000;

    private PlaylistFragment mPlaylistFragment;
    private DisconnectedFragment mDisconnectedFragment;
    private ConnectingFragment mConnectingFragment;
    private PlayerControllerFragment mPlayerControllerFragment;
    private MenuItem mConnectMenuItem;
    private MenuItem mDisconnectMenuItem;

    private PlaylistClientService.LocalBinder mPlaylistServiceBinder;
    private ClientPlaylistController mClientPlaylistController;
    private MediaPlayerController mMediaPlayerController;
    private ConnectionState mConnectionState;
    private Handler mMainHandler;
    private Runnable mReconnectionTimeoutRunnable;

    private enum ConnectionState {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        RECONNECTING
    }

    private ServiceConnection mPlaylistServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mPlaylistServiceBinder = (PlaylistClientService.LocalBinder) service;

            if (mConnectionState == ConnectionState.DISCONNECTED) {
                // The service has restarted after being killed by the OS but the time allowed to reconnect
                // has elapsed or the user has manually cancelled the reconnection
                mPlaylistServiceBinder.disconnect();
                return;
            }

            mPlaylistServiceBinder.setConnectionCallback(mPlaylistServiceCallback);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // The service has been killed by the OS (if the service had crashed, the activity would have also
            // crashed since it runs in the same thread).
            // The service will therefore be restarted by the OS and we just have to wait for that restart...

            setConnectionState(ConnectionState.RECONNECTING);

            mPlaylistServiceBinder = null;

            mReconnectionTimeoutRunnable = new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getContext(), R.string.reconnection_failed, Toast.LENGTH_LONG).show();
                    cancelConnection();
                }
            };

            mMainHandler.postDelayed(mReconnectionTimeoutRunnable, RECONNECTION_DELAY);
        }
    };

    private PlaylistClientService.ConnectionCallback mPlaylistServiceCallback = new PlaylistClientService.ConnectionCallback() {
        @Override
        public void onConnected(@NonNull ClientPlaylistController playlistController, @Nullable MediaPlayerController playerController) {
            setPlaylistController(playlistController);

            if (playerController != null) {
                mPlayerControllerFragment = PlayerControllerFragment.newInstance();

                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down);
                transaction.add(R.id.player_fragment_container, mPlayerControllerFragment, PlayerControllerFragment.TAG);
                transaction.commit();
            }

            mMediaPlayerController = playerController;

            //mPlaylistFragment.onConnected(playlistController);

            setConnectionState(ConnectionState.CONNECTED);
        }

        @Override
        public void onConnectionFailed(ConnectionFailedReason reason) {
            Toast.makeText(getContext(), getString(R.string.on_connection_failed, getText(reason.getMessageResId())), Toast.LENGTH_LONG).show();

            requireContext().unbindService(mPlaylistServiceConnection);
            mPlaylistServiceBinder = null;

            setConnectionState(ConnectionState.DISCONNECTED);
        }

        /*@Override
        public void onKicked() {
            Toast.makeText(getContext(), R.string.on_kicked, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onBanned() {
            Toast.makeText(getContext(), R.string.on_banned, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onServerStopped() {
            Toast.makeText(getContext(), R.string.on_server_stopped, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onConnectionLost() {
            Toast.makeText(getContext(), R.string.connection_lost, Toast.LENGTH_LONG).show();
        }*/

        @Override
        public void onDisconnected() {
            setConnectionState(ConnectionState.DISCONNECTED);
            unbindService();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);

        mMainHandler = new Handler(Looper.getMainLooper());
        mConnectionState = ConnectionState.DISCONNECTED;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_client, container, false);

        FragmentManager childFragmentManager = getChildFragmentManager();

        // If the enqueue dialog was opened by the PlaylistFragment when configuration has changed,
        // the PlaylistFragment (which is the target of the dialog) has been saved and restored and
        // we must then use that restored instance.
        mPlaylistFragment = (PlaylistFragment) childFragmentManager.findFragmentByTag(PlaylistFragment.TAG);

        if (mPlaylistFragment == null)
            mPlaylistFragment = PlaylistFragment.newInstance();

        mDisconnectedFragment = DisconnectedFragment.newInstance();
        mConnectingFragment = ConnectingFragment.newInstance();

        PlayerControllerFragment playerControllerFragment = (PlayerControllerFragment) childFragmentManager.findFragmentByTag(PlayerControllerFragment.TAG);

        // If the fragment was saved, removes it as we need to reconnect to the service before being able to show this fragment
        if (playerControllerFragment != null) {
            FragmentTransaction transaction = childFragmentManager.beginTransaction();
            transaction.remove(playerControllerFragment);
            transaction.commitNow();
        }

        Context context = requireContext();

        if (PlaylistServiceRunningChecker.checkRunning(context)) {
            setConnectionState(ConnectionState.CONNECTING);

            Intent intent = new Intent(context, PlaylistClientService.class);
            context.bindService(intent, mPlaylistServiceConnection, Context.BIND_AUTO_CREATE);
        } else {
            setConnectionState(ConnectionState.DISCONNECTED);
        }

        return rootView;
    }

    @Override
    public void onDestroyView() {
        if (mPlaylistServiceBinder != null)
            unbindService();

        super.onDestroyView();
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        mConnectMenuItem = menu.findItem(R.id.action_connect);
        mDisconnectMenuItem = menu.findItem(R.id.action_disconnect);

        switch (mConnectionState) {
            case CONNECTING:
            case RECONNECTING:
                mConnectMenuItem.setEnabled(false);
                break;
            case CONNECTED:
                mConnectMenuItem.setVisible(false);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_connect:
                showConnectionDialog();
                break;
            case R.id.action_disconnect:
                mPlaylistServiceBinder.disconnect();
                break;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onConnectionRequested(ConnectionInfo connectionInfo) {
        Context context = requireContext();

        Intent intent = new Intent(context, PlaylistClientService.class);
        intent.putExtra(PlaylistClientService.EXTRA_CONNECTION_INFO, connectionInfo);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            context.startForegroundService(intent);
        else
            context.startService(intent);

        context.bindService(intent, mPlaylistServiceConnection, Context.BIND_AUTO_CREATE);

        setConnectionState(ConnectionState.CONNECTING);
    }

    private void unbindService() {
        mPlaylistServiceBinder.setConnectionCallback(null);

        requireContext().unbindService(mPlaylistServiceConnection);

        if (mConnectionState == ConnectionState.CONNECTED) // Can also be CONNECTING if the user has cancelled the connection
            mPlaylistFragment.onDisconnected();

        if (mPlayerControllerFragment != null) {
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.remove(mPlayerControllerFragment);
            transaction.commitAllowingStateLoss();
            mPlayerControllerFragment = null;
        }

        mPlaylistServiceBinder = null;
        setPlaylistController(null);
        mMediaPlayerController = null;
    }

    private void setConnectionState(ConnectionState state) {
        mConnectionState = state;

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

        switch (mConnectionState) {
            case DISCONNECTED:
                transaction.replace(R.id.fragment_container, mDisconnectedFragment);
                break;
            case CONNECTING:
                mConnectingFragment.setReconnecting(false);
                transaction.replace(R.id.fragment_container, mConnectingFragment);
                break;
            case CONNECTED:
                transaction.replace(R.id.fragment_container, mPlaylistFragment, PlaylistFragment.TAG);
                break;
            case RECONNECTING:
                mConnectingFragment.setReconnecting(true);
                transaction.replace(R.id.fragment_container, mConnectingFragment);
                break;
        }

        transaction.commitAllowingStateLoss();

        if (mConnectMenuItem == null)
            return; // Menu has not already been created

        switch (mConnectionState) {
            case DISCONNECTED:
                mConnectMenuItem.setVisible(true);
                mConnectMenuItem.setEnabled(true);
                mDisconnectMenuItem.setVisible(false);
                break;
            case CONNECTING:
                mConnectMenuItem.setEnabled(false);
                break;
            case CONNECTED:
                mConnectMenuItem.setVisible(false);
                mDisconnectMenuItem.setVisible(true);
                break;
        }
    }
	
	private void setPlaylistController(@Nullable ClientPlaylistController playlistController) {
		mClientPlaylistController = playlistController;
		
		((MainActivity) requireActivity()).setPlaylistController(playlistController);
	}

    ClientPlaylistController getPlaylistController() {
        return mClientPlaylistController;
    }

    MediaPlayerController getPlayerController() {
        return mMediaPlayerController;
    }
	
	void kickUser(String userName) {
		if (mConnectionState != ConnectionState.CONNECTED)
			throw new IllegalStateException("Not connected to server.");
		
		mPlaylistServiceBinder.kickUser(userName);
	}
	
	void banUser(String userName) {
		if (mConnectionState != ConnectionState.CONNECTED)
			throw new IllegalStateException("Not connected to server.");
		
		mPlaylistServiceBinder.banUser(userName);
	}

    void cancelConnection() {
        switch (mConnectionState) {
            case CONNECTING:
                mPlaylistServiceBinder.disconnect();
                mPlaylistServiceBinder = null;
                break;
            case RECONNECTING:
                mMainHandler.removeCallbacks(mReconnectionTimeoutRunnable);
                mReconnectionTimeoutRunnable = null;
                mPlaylistServiceCallback.onDisconnected();
                break;
            default:
                throw new IllegalStateException("Client is not connecting or reconnecting.");
        }
    }

    void updateOptionMenu(@NonNull Menu menu) {
        mConnectMenuItem = menu.findItem(R.id.action_connect);
        mDisconnectMenuItem = menu.findItem(R.id.action_disconnect);

        switch (mConnectionState) {
            case CONNECTING:
            case RECONNECTING:
                mConnectMenuItem.setEnabled(false);
                break;
            case CONNECTED:
                mConnectMenuItem.setVisible(false);
                mDisconnectMenuItem.setVisible(true);
                break;
        }
    }

    void showConnectionDialog() {
        ConnectToServerDialog dialog = new ConnectToServerDialog();
        dialog.setTargetFragment(this, 0);
        dialog.show(requireFragmentManager(), ConnectToServerDialog.TAG);
    }

    void disconnect() {
        if (mPlaylistServiceBinder == null)
            return;

        mPlaylistServiceBinder.disconnect();
    }

    public static ClientFragment newInstance() {
        return new ClientFragment();
    }
}
