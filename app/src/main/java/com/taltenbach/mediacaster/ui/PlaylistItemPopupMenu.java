package com.taltenbach.mediacaster.ui;

import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.client.ClientPlaylistController;
import com.taltenbach.mediacaster.model.Playlist;
import com.taltenbach.mediacaster.model.PlaylistEntry;

import java.lang.ref.WeakReference;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 14/08/2018
 */

public final class PlaylistItemPopupMenu extends ItemPopupMenu {
    private final Button mPlayNowButton;
	private final Button mVoteToSkipButton;
	private final Button mDeleteButton;
	private final Button mKickUserButton;
	private final Button mBanUserButton;

	private final WeakReference<PlaylistFragment> mPlaylistFragment;
	private final ClientPlaylistController mPlaylistController;
	
	private PlaylistEntry mEntry;
	private int mEntryPos;
	
	private final Playlist.CurrentEntryChangeObserver mCurrentEntryChangedObserver = new Playlist.CurrentEntryChangeObserver() {
		@Override
		public void onCurrentEntryChanged(int newEntryIndex, PlaylistEntry newCurrentEntry) {
		    setCurrentEntry(mEntry.equals(newCurrentEntry));
		}
	};
	
	public PlaylistItemPopupMenu(@NonNull PlaylistFragment playlistFragment, @NonNull ClientPlaylistController playlistController) {
		super(playlistFragment.requireContext(), R.layout.menu_playlist_item);
		
		mPlaylistFragment = new WeakReference<>(playlistFragment);
		mPlaylistController = playlistController;

		setOnDismissListener(new PopupWindow.OnDismissListener() {
			@Override
			public void onDismiss() {
				mPlaylistController.getPlaylist().removeCurrentEntryChangeObserver(mCurrentEntryChangedObserver);
				mPlaylistController.setUserSkipVoteObserver(null);
				mEntry = null;
			}
		});
		
		View contentView = getContentView();

		mPlayNowButton = contentView.findViewById(R.id.btn_play_now);
		mVoteToSkipButton = contentView.findViewById(R.id.btn_vote_to_skip);
		mDeleteButton = contentView.findViewById(R.id.btn_delete);
        mKickUserButton = contentView.findViewById(R.id.btn_kick_user);
        mBanUserButton = contentView.findViewById(R.id.btn_ban_user);

        mPlayNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlaylistController.requestPlayOf(mEntry, mEntryPos);
                dismiss();
            }
        });

		mVoteToSkipButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mPlaylistController.requestVoteToSkipCurrentEntry();
				dismiss();
			}
		});
		
		mDeleteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				PlaylistFragment playlistFragment = mPlaylistFragment.get();
				
				if (playlistFragment != null)
					playlistFragment.removeEntry(mEntry, mEntryPos);
				
				dismiss();
			}
		});
		
		if (!playlistController.isUserAdmin())
			return;

        mKickUserButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				PlaylistFragment playlistFragment = mPlaylistFragment.get();
				
				if (playlistFragment != null) {
					playlistFragment.kickUser(mEntry.getAdderName());
					Toast.makeText(v.getContext(), R.string.kick_request_sent, Toast.LENGTH_SHORT).show();
				}
				
				dismiss();
			}
		});

        mBanUserButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				PlaylistFragment playlistFragment = mPlaylistFragment.get();
				
				if (playlistFragment != null) {
					playlistFragment.banUser(mEntry.getAdderName());
					Toast.makeText(v.getContext(), R.string.ban_request_sent, Toast.LENGTH_SHORT).show();
				}
				
				dismiss();
			}
		});
	}
	
	public void showAt(@NonNull View itemView, float x, float y, @NonNull PlaylistEntry entry, int entryPos) {
		mEntry = entry;
		mEntryPos = entryPos;
		
		Playlist playlist = mPlaylistController.getPlaylist();

		setCurrentEntry(entryPos == playlist.getCurrentEntryIndex());
		playlist.addCurrentEntryChangeObserver(mCurrentEntryChangedObserver);
		
		mDeleteButton.setEnabled(mPlaylistController.canRemovePlaylistEntry(entry));

		if (mPlaylistController.isUserAdmin() && !entry.getAdderName().equals(mPlaylistController.getUserName()) && !entry.getAdderName().equals("<Server>")) {
            mKickUserButton.setVisibility(View.VISIBLE);
            mBanUserButton.setVisibility(View.VISIBLE);
        } else {
            mKickUserButton.setVisibility(View.GONE);
            mBanUserButton.setVisibility(View.GONE);
        }

		showAt(itemView, x, y, entry.getMedia().getTitle());
	}

    private void setCurrentEntry(boolean isCurrentEntry) {
        if (isCurrentEntry) {
            mVoteToSkipButton.setVisibility(View.VISIBLE);

            if (mPlaylistController.canVoteToSkipCurrentEntry()) {
				mPlaylistController.setUserSkipVoteObserver(new ClientPlaylistController.UserSkipVoteObserver() {
					@Override
					public void onUserVotedToSkipCurrentEntry() {
						mVoteToSkipButton.setEnabled(false);
						mPlaylistController.setUserSkipVoteObserver(null);
					}
				});

				mVoteToSkipButton.setEnabled(true);
            } else {
				mVoteToSkipButton.setEnabled(false);
            }

            mPlayNowButton.setVisibility(View.GONE);
        } else {
            mVoteToSkipButton.setVisibility(View.GONE);
            mPlayNowButton.setVisibility(mPlaylistController.isUserAdmin() ? View.VISIBLE : View.GONE);
        }
    }
}
