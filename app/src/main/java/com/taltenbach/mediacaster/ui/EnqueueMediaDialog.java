package com.taltenbach.mediacaster.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.client.ConnectionInfo;
import com.taltenbach.mediacaster.model.PlatformMedia;
import com.taltenbach.mediacaster.util.validation.GlobalTextValidator;
import com.taltenbach.mediacaster.util.validation.TextValidator;
import com.taltenbach.mediacaster.util.validation.ValidationObserver;
import com.taltenbach.mediacaster.util.validation.Validator;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 26/07/2018
 */

public class EnqueueMediaDialog extends DialogFragment {
    public static final String TAG = "EnqueueMediaDialog";

    private EnqueueMediaDialogObserver mObserver;

    public interface EnqueueMediaDialogObserver {
        void onEnqueueRequested(String mediaUrl);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            Fragment targetFragment = getTargetFragment();

            if (targetFragment == null)
                mObserver = (EnqueueMediaDialogObserver) context;
            else
                mObserver = (EnqueueMediaDialogObserver) targetFragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " or the target fragment must implement EnqueueMediaObserver");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        @SuppressLint("InflateParams") // Pass null as the parent view because its going in the dialog layout
        View contentView = inflater.inflate(R.layout.dialog_enqueue_media, null);

        final EditText urlField = contentView.findViewById(R.id.input_url);

        final TextValidator urlValidator = TextValidator.watch(urlField, R.string.invalid_media_url, new Validator<String>() {
            @Override
            public boolean isValid(String value) {
                return PlatformMedia.isValidMediaUrl(value);
            }
        });

        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.enqueue_media)
                .setView(contentView)
                .setPositiveButton(R.string.enqueue, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mObserver.onEnqueueRequested(urlField.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
                final Button enqueueButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                enqueueButton.setEnabled(urlValidator.isValid());

                urlValidator.setValidationObserver(new ValidationObserver() {
                    @Override
                    public void onValidationChanged(boolean isValid) {
                        enqueueButton.setEnabled(isValid);
                    }
                });
            }
        });

        return dialog;
    }
}
