package com.taltenbach.mediacaster.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.client.ConnectionInfo;
import com.taltenbach.mediacaster.util.validation.GlobalTextValidator;
import com.taltenbach.mediacaster.util.validation.ValidationObserver;
import com.taltenbach.mediacaster.util.validation.Validator;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 23/07/2018
 */

public class ConnectToServerDialog extends DialogFragment {
    public static final String TAG = "ConnectToServerDialog";

    private static final String KEY_USERNAME = "com.taltenbach.mediacaster.ui.ConnectToServerDialog.KEY_USERRNAME";
    private static final String KEY_IP = "com.taltenbach.mediacaster.ui.ConnectToServerDialog.KEY_IP";
    private static final String KEY_PORT = "com.taltenbach.mediacaster.ui.ConnectToServerDialog.KEY_PORT";

    private EditText mUsername;
    private EditText mIp;
    private EditText mPort;
    private EditText mAdminPassword;

    private ConnectToServerDialogObserver mObserver;

    public interface ConnectToServerDialogObserver {
        void onConnectionRequested(ConnectionInfo connectionInfo);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            Fragment targetFragment = getTargetFragment();

            if (targetFragment == null)
                mObserver = (ConnectToServerDialogObserver) context;
            else
                mObserver = (ConnectToServerDialogObserver) targetFragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " or the target fragment must implement ConnectToServerDialogObserver");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        @SuppressLint("InflateParams") // Pass null as the parent view because its going in the dialog layout
        View contentView = inflater.inflate(R.layout.dialog_connect, null);

        mUsername = contentView.findViewById(R.id.input_username);
        mIp = contentView.findViewById(R.id.input_ip);
        mPort = contentView.findViewById(R.id.input_port);
        mAdminPassword = contentView.findViewById(R.id.input_admin_password);

        if (savedInstanceState == null) {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);

            mUsername.setText(sharedPrefs.getString(KEY_USERNAME, ""));
            mIp.setText(sharedPrefs.getString(KEY_IP, ""));
            mPort.setText(sharedPrefs.getString(KEY_PORT, ""));
        }

        final GlobalTextValidator globalTextValidator = new GlobalTextValidator();

        globalTextValidator.addField(mUsername, R.string.empty_username, new Validator<String>() {
                @Override
                public boolean isValid(String value) {
                    return !value.isEmpty();
                }
            })
            .addField(mIp, R.string.invalid_ip, new Validator<String>() {
                @Override
                public boolean isValid(String value) {
                    return value.matches("^((25[0-5]|2[0-4]\\d|[01]?\\d\\d?).){3}(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)$");
                }
            })
            .addField(mPort, R.string.invalid_port, new Validator<String>() {
                @Override
                public boolean isValid(String value) {
                    int port;

                    try {
                        port = Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        return false;
                    }

                    return port >= 0 && port <= 65535;
                }
            });

        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.connect_to_server)
                .setView(contentView)
                .setPositiveButton(R.string.connect, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onConnectButtonClick();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
                final Button connectButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                connectButton.setEnabled(globalTextValidator.isValid());

                globalTextValidator.setValidationObserver(new ValidationObserver() {
                    @Override
                    public void onValidationChanged(boolean isValid) {
                        connectButton.setEnabled(isValid);
                    }
                });
            }
        });

        return dialog;
    }

    private void onConnectButtonClick() {
        String username = mUsername.getText().toString();
        String ip = mIp.getText().toString();
        String adminPassword = mAdminPassword.getText().toString();
        String port = mPort.getText().toString();

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_IP, ip);
        editor.putString(KEY_PORT, port);
        editor.apply();

        ConnectionInfo info = new ConnectionInfo(ip, Integer.parseInt(port), username, adminPassword);
        mObserver.onConnectionRequested(info);
    }
}
