package com.taltenbach.mediacaster.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.service.voice.VoiceInteractionService;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.taltenbach.mediacaster.R;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 23/07/2018
 */

public class RecyclerViewEmptySupport extends RecyclerView {
    private int mEmptyViewId;
    private View mEmptyView;

    //private boolean mVisible;

    private AdapterDataObserver emptyObserver = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            checkIfEmpty();
        }
    };

    public RecyclerViewEmptySupport(Context context) {
        this(context, null);
    }

    public RecyclerViewEmptySupport(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecyclerViewEmptySupport(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        //mVisible = getVisibility() == VISIBLE;

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.RecyclerViewEmptySupport, defStyle, 0);

        try {
            mEmptyViewId = a.getResourceId(R.styleable.RecyclerViewEmptySupport_emptyView, 0);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        if (mEmptyViewId != 0)
            setEmptyView(((View) getParent()).findViewById(mEmptyViewId));
    }

    @Override
    public void setAdapter(Adapter adapter) {
        Adapter<?> oldAdapter = getAdapter();

        if (oldAdapter != null)
            oldAdapter.unregisterAdapterDataObserver(emptyObserver);

        super.setAdapter(adapter);

        if (adapter != null)
            adapter.registerAdapterDataObserver(emptyObserver);

        checkIfEmpty();
    }

    public void setEmptyView(View emptyView) {
        mEmptyView = emptyView;

        checkIfEmpty();
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);

        if (visibility == VISIBLE)
            checkIfEmpty();
        else
            mEmptyView.setVisibility(GONE);

        /*if (visibility == VISIBLE) {
            mVisible = true;
            checkIfEmpty();
        } else {
            mVisible = false;
            mEmptyView.setVisibility(GONE);
        }*/
    }

    private void checkIfEmpty() {
        Adapter<?> adapter = getAdapter();

        if (getVisibility() != VISIBLE || adapter == null || mEmptyView == null)
            return;

        mEmptyView.setVisibility(adapter.getItemCount() == 0 ? VISIBLE : GONE);

        // Leaving the RecyclerView always visible avoid breaking deletion animation when there is only one item left in the list
        /*if (!mVisible || adapter == null || mEmptyView == null)
            return;

        if (adapter.getItemCount() == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
            RecyclerViewEmptySupport.super.setVisibility(View.INVISIBLE); // Use INVISIBLE instead of GONE to avoid breaking insertion animation when list is empty
        } else {
            mEmptyView.setVisibility(View.GONE);
            RecyclerViewEmptySupport.super.setVisibility(View.VISIBLE);
        }*/
    }
}
