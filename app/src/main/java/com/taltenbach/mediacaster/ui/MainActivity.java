package com.taltenbach.mediacaster.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.client.ClientPlaylistController;
import com.taltenbach.mediacaster.util.MutableObservableValue;
import com.taltenbach.mediacaster.util.ObservableValue;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int TAB_PLAYLIST = 0;
    private static final int TAB_FAVORITE = 1;

    private ViewPagerAdapter mViewPagerAdapter;

	private MutableObservableValue<ClientPlaylistController> mPlaylistController;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		mPlaylistController = new MutableObservableValue<>();
		
        ViewModelProviders.of(this).get(FavoriteViewModel.class)
                .loadFavorites(null);

        setContentView(R.layout.activity_main);

        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        ViewPager viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(mViewPagerAdapter);
        viewPager.setOffscreenPageLimit(1);

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);

        //noinspection ConstantConditions
        tabLayout.getTabAt(TAB_PLAYLIST).setIcon(R.drawable.ic_playlist_selector);
        //noinspection ConstantConditions
        tabLayout.getTabAt(TAB_FAVORITE).setIcon(R.drawable.ic_favorite_tab_selector);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_playlist, menu);

        getClientFragment().updateOptionMenu(menu);

        //menu.setGroupVisible(R.id.group_favorite, mViewPager.getCurrentItem() == TAB_FAVORITE);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_connect:
                getClientFragment().showConnectionDialog();
                break;
            case R.id.action_disconnect:
                getClientFragment().disconnect();
                break;
            case R.id.action_app_info:
                AppInfoDialog.newInstance().show(getSupportFragmentManager(), AppInfoDialog.TAG);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    ObservableValue<ClientPlaylistController> getPlaylistController() {
		return mPlaylistController;
	}
	
	void setPlaylistController(@Nullable ClientPlaylistController playlistController) {
		mPlaylistController.setValue(playlistController);
	}

	private ClientFragment getClientFragment() {
        return (ClientFragment) mViewPagerAdapter.getItem(TAB_PLAYLIST);
    }

    private static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final Fragment[] mFragments;

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);

            mFragments = new Fragment[2];
        }

        @Override
        public Fragment getItem(int position) {
            if (mFragments[0] == null) { // Fragments have not been restored from a previously saved state
                mFragments[TAB_PLAYLIST] = ClientFragment.newInstance();
                mFragments[TAB_FAVORITE] = FavoritesFragment.newInstance();
            }

            return mFragments[position];
        }

        @Override
        public int getCount() {
            return mFragments.length;
        }

        @NonNull
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
            mFragments[position] = createdFragment;
            return createdFragment;
        }
    }
}
