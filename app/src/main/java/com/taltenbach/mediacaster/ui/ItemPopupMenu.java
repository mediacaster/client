package com.taltenbach.mediacaster.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.icu.util.Measure;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.taltenbach.mediacaster.R;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 14/08/2018
 */

public abstract class ItemPopupMenu {
    private final PopupWindow mPopupWindow;
	private final TextView mTitleView;
	
	protected ItemPopupMenu(@NonNull Context c, @LayoutRes int contentResId) {
		LayoutInflater inflater = LayoutInflater.from(c);

		@SuppressLint("InflateParams")
		View contentView = inflater.inflate(contentResId, null);

		mTitleView = contentView.findViewById(R.id.title);

		mPopupWindow = new PopupWindow(contentView, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, true);
		mPopupWindow.setBackgroundDrawable(new ColorDrawable());
	}
	
	protected View getContentView() {
		return mPopupWindow.getContentView();
	}

    protected void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
        mPopupWindow.setOnDismissListener(onDismissListener);
    }

    /**
     * Show the popup menu anchored to the specified item view, at the given relative coordinates
     * @param itemView The item view that will be used as anchor to show the popup
     * @param x The x coordinate relative to the top-left corner of the item view at which to show the popup
     * @param y The y coordinate relative to the top-left corner of the item view at which to show the popup
     * @param title The title of the popup
     */
	protected void showAt(@NonNull View itemView, float x, float y, String title) {
		mTitleView.setText(title);

        WindowManager windowManager = (WindowManager) itemView.getContext().getSystemService(Context.WINDOW_SERVICE);
        //noinspection ConstantConditions
        Display display = windowManager.getDefaultDisplay();

        Point displaySize = new Point();
        display.getSize(displaySize);

        int[] itemViewLocation = new int[2];
        itemView.getLocationOnScreen(itemViewLocation);

        View popupContentView = mPopupWindow.getContentView();
        popupContentView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        int popupHeight = popupContentView.getMeasuredHeight();

        if (itemViewLocation[1] + popupHeight < 2 * displaySize.y / 3)
            mPopupWindow.showAsDropDown(itemView, (int) x, (int) (y - itemView.getHeight()));
        else // Open the menu to the top if the bottom of the popup menu is after the 2/3rd of the screen size
            mPopupWindow.showAsDropDown(itemView, (int) x, (int) (y - popupHeight - itemView.getHeight()));
	}
	
	public void dismiss() {
		mPopupWindow.dismiss();
	}
}
