package com.taltenbach.mediacaster.ui;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.animation.OvershootInterpolator;
import android.widget.ToggleButton;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 10/08/2018
 */

public final class FavoriteButton extends ToggleButton {
    private static final int BOUNCE_ANIM_DURATION = 300;
    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator();

    @RequiresApi(21)
    public FavoriteButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public FavoriteButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public FavoriteButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FavoriteButton(Context context) {
        super(context);
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);

        float bounceStart = checked ? 0.2f : 1.3f;

        ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat(this, "scaleX", bounceStart, 1.0f);
        bounceAnimX.setDuration(BOUNCE_ANIM_DURATION);
        bounceAnimX.setInterpolator(OVERSHOOT_INTERPOLATOR);

        ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat(this, "scaleY", bounceStart, 1.0f);
        bounceAnimY.setDuration(BOUNCE_ANIM_DURATION);
        bounceAnimY.setInterpolator(OVERSHOOT_INTERPOLATOR);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(bounceAnimX, bounceAnimY);
        animatorSet.start();
    }
}
