package com.taltenbach.mediacaster.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.client.ClientPlaylistController;
import com.taltenbach.mediacaster.client.ConnectionFailedReason;
import com.taltenbach.mediacaster.client.PlaylistClientService;
import com.taltenbach.mediacaster.model.PlatformMedia;
import com.taltenbach.mediacaster.util.PlaylistServiceRunningChecker;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 10/08/2018
 */

public class EnqueueMediaActivity extends Activity {
    private ServiceConnection mPlaylistServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PlaylistClientService.LocalBinder binder = (PlaylistClientService.LocalBinder) service;

            ClientPlaylistController playlistController = binder.getPlaylistController();
			
			if (playlistController != null) {
				playlistController.requestPlatformMediaEnqueue(getIntent().getStringExtra(Intent.EXTRA_TEXT));

				Toast.makeText(EnqueueMediaActivity.this, R.string.enqueue_request_sent, Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(EnqueueMediaActivity.this, R.string.error_not_connected, Toast.LENGTH_LONG).show();
			}
			
            unbindService(mPlaylistServiceConnection);
            finish();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // Empty
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        String url = intent.getStringExtra(Intent.EXTRA_TEXT);

        if (intent.getAction() == null || !intent.getAction().equals(Intent.ACTION_SEND) || url == null) {
            finish();
            return;
        }

        if (!PlatformMedia.isValidMediaUrl(url)) {
            Toast.makeText(this, R.string.invalid_media_url, Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        if (!PlaylistServiceRunningChecker.checkRunning(this)) {
            Toast.makeText(this, R.string.error_not_connected, Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        Intent serviceIntent = new Intent(this, PlaylistClientService.class);
        bindService(serviceIntent, mPlaylistServiceConnection, Context.BIND_AUTO_CREATE);
    }
}
