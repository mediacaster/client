package com.taltenbach.mediacaster.ui;

import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.client.ClientPlaylistController;
import com.taltenbach.mediacaster.model.PlatformMedia;
import com.taltenbach.mediacaster.util.ObservableValue;

import java.lang.ref.WeakReference;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 14/08/2018
 */

public final class FavoriteItemPopupMenu extends ItemPopupMenu {
	private final Button mAddToPlaylistButton;
	
	private final WeakReference<FavoritesFragment> mFavoriteFragment;
	private final ObservableValue<ClientPlaylistController> mPlaylistController;
	
	private PlatformMedia mMedia;
	private int mMediaPos;
	
	private final ObservableValue.ValueChangeObserver<ClientPlaylistController> mPlaylistControllerChangeObserver = new ObservableValue.ValueChangeObserver<ClientPlaylistController>() {
		@Override
			public void onValueChanged(ClientPlaylistController newValue) {
				mAddToPlaylistButton.setEnabled(newValue != null);
			}
		};
	
	public FavoriteItemPopupMenu(@NonNull FavoritesFragment favoriteFragment, @NonNull ObservableValue<ClientPlaylistController> playlistController) {
		super(favoriteFragment.requireContext(), R.layout.menu_favorite_item);
		
		mFavoriteFragment = new WeakReference<>(favoriteFragment);
		mPlaylistController = playlistController;

        setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mPlaylistController.removeValueChangeObserver(mPlaylistControllerChangeObserver);
                mMedia = null;
            }
        });

		View contentView = getContentView();
		
		mAddToPlaylistButton = contentView.findViewById(R.id.btn_add_to_playlist);
		
		mAddToPlaylistButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mPlaylistController.getValue().requestPlatformMediaEnqueue(mMedia.getPlatform(), mMedia.getPlatformKey());
				dismiss();
			}
		});
		
		contentView.findViewById(R.id.btn_delete).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FavoritesFragment favoriteFragment = mFavoriteFragment.get();
				
				if (favoriteFragment != null)
					favoriteFragment.removeFavorite(mMediaPos);
				
				dismiss();
			}
		});
	}
	
	public void showAt(@NonNull View itemView, float x, float y, @NonNull PlatformMedia media, int mediaPos) {
		mMedia = media;
		mMediaPos = mediaPos;
		
		mPlaylistController.addValueChangeObserver(mPlaylistControllerChangeObserver);
		
		mAddToPlaylistButton.setEnabled(mPlaylistController.getValue() != null);
		
		showAt(itemView, x, y, media.getTitle());
	}
}