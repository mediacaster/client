package com.taltenbach.mediacaster.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.model.Favorites;
import com.taltenbach.mediacaster.util.ResultCallback;

public class FavoritesFragment extends Fragment {
    public static final int TITLE_RES_ID = R.string.favorites_fragment_title;

    private View mRootView;
    private RecyclerView mFavoriteListView;
    private Favorites mFavorites;

    private final BroadcastReceiver mNetworkStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            RecyclerView.Adapter favoriteListAdapter = mFavoriteListView.getAdapter();

            if (intent == null || intent.getExtras() == null || favoriteListAdapter == null)
                return;

            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            //noinspection ConstantConditions
            NetworkInfo networkInfo = manager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED)
                favoriteListAdapter.notifyDataSetChanged();
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_favorites, container, false);

        Context context = requireContext();

        mFavoriteListView = mRootView.findViewById(R.id.favorite_list_view);
        mFavoriteListView.setLayoutManager(new LinearLayoutManager(context));

        FavoriteViewModel favoritesViewModel = ViewModelProviders.of(requireActivity()).get(FavoriteViewModel.class);

        favoritesViewModel.loadFavorites(new ResultCallback<Favorites>() {
            @Override
            public void onResult(Favorites favorites) {
                mFavorites = favorites;
				
				MainActivity mainActivity = (MainActivity) requireActivity();
				
                mFavoriteListView.setAdapter(new FavoriteListAdapter(FavoritesFragment.this, favorites, mainActivity.getPlaylistController()));

                mRootView.findViewById(R.id.loading_view).setVisibility(View.GONE);
                mFavoriteListView.setVisibility(View.VISIBLE);
            }
        });

        new ItemTouchHelper(new SwipeToDeleteCallback(context, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                removeFavorite(viewHolder.getAdapterPosition());
            }
        }).attachToRecyclerView(mFavoriteListView);

        context.registerReceiver(mNetworkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        return mRootView;
    }
	
	void removeFavorite(int pos) {
		final Snackbar snackbar = Snackbar.make(mRootView, R.string.deleted, Snackbar.LENGTH_LONG)
				.setAction(R.string.undo, new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						mFavorites.restoreRemoved();
					}
				})
				.addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
					@Override
					public void onDismissed(Snackbar transientBottomBar, int event) {
						if (event != BaseTransientBottomBar.BaseCallback.DISMISS_EVENT_ACTION && event != DISMISS_EVENT_MANUAL)
							mFavorites.commitRemove();
					}
				});

		mFavorites.remove(pos, new Favorites.RemovalCancelledCallback() {
			@Override
			public void onRemoveCancelled() {
				snackbar.dismiss();
			}
		});

		snackbar.show();
	}

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        requireContext().unregisterReceiver(mNetworkStateReceiver);
    }

    public static FavoritesFragment newInstance() {
        return new FavoritesFragment();
    }
}
