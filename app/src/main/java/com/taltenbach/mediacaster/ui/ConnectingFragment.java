package com.taltenbach.mediacaster.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.taltenbach.mediacaster.R;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 26/07/2018
 */

public class ConnectingFragment extends Fragment {
    private static final String KEY_IS_RECONNECTING = "IS_RECONNECTING";

    private TextView mConnectingText;

    private boolean mIsReconnecting;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_connecting, container, false);

        mConnectingText = rootView.findViewById(R.id.text_connecting);

        Button cancelButton = rootView.findViewById(R.id.btn_cancel);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClientFragment clientFragment = (ClientFragment) getParentFragment();

                if (clientFragment == null)
                    throw new IllegalStateException("This fragment must have a ClientFragment as parent.");

                clientFragment.cancelConnection();
            }
        });

        if (savedInstanceState != null)
            mIsReconnecting = savedInstanceState.getBoolean(KEY_IS_RECONNECTING);

        setReconnecting(mIsReconnecting);

        return rootView;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(KEY_IS_RECONNECTING, mIsReconnecting);
    }

    void setReconnecting(boolean isReconnecting) {
        mIsReconnecting = isReconnecting;

        if (mConnectingText != null)
            mConnectingText.setText(isReconnecting ? R.string.reconnecting : R.string.connecting);
    }

    public static ConnectingFragment newInstance() {
        return new ConnectingFragment();
    }
}
