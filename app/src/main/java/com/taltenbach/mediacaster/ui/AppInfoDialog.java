package com.taltenbach.mediacaster.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.taltenbach.mediacaster.R;


/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 07/09/2018
 */

public class AppInfoDialog extends DialogFragment {
    public static final String TAG = "AppInfoDialog";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.dialog_app_info, container, false);

        String appVersion = null;

        try {
            Context context = requireContext();
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(getClass().getSimpleName(), "Unable to get app version.", e);
        }

        TextView appVersionView = contentView.findViewById(R.id.app_version);
        appVersionView.setMovementMethod(LinkMovementMethod.getInstance());
        appVersionView.setText(fromHtmlCompat(String.format(getString(R.string.app_version), appVersion)));

        return contentView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @SuppressWarnings("deprecation")
    private static Spanned fromHtmlCompat(@NonNull String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public static AppInfoDialog newInstance() {
        return new AppInfoDialog();
    }
}
