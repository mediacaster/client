package com.taltenbach.mediacaster.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.client.MediaPlayerController;

import java.util.Locale;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 21/08/2018
 */

public class PlayerControllerFragment extends Fragment {
    public static final String TAG = "PlayerControllerFragment";

    private ImageButton mPlayPauseButton;
    private ImageButton mSkipPreviousButton;
    private ImageButton mSkipNextButton;
    private ProgressBar mLoadingBar;
    private SeekBar mCurrentMediaTimeBar;
    private SeekBar mRemoteVolumeBar;
    private TextView mCurrentMediaTimeView;
    private TextView mMediaDurationView;

    private Drawable mPlayDrawable;
    private Drawable mPauseDrawable;

    private MediaPlayerController mPlayerController;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_player_controller, container, false);

        Context context = requireContext();
        mPlayDrawable = ContextCompat.getDrawable(context, R.drawable.ic_play_arrow_selector);
        mPauseDrawable = ContextCompat.getDrawable(context, R.drawable.ic_pause_white_48);

        mPlayPauseButton = rootView.findViewById(R.id.btn_play_pause);
        mSkipPreviousButton = rootView.findViewById(R.id.btn_skip_previous);
        mSkipNextButton = rootView.findViewById(R.id.btn_skip_next);
        mLoadingBar = rootView.findViewById(R.id.bar_loading);
        mCurrentMediaTimeBar = rootView.findViewById(R.id.bar_current_media_time);
        mRemoteVolumeBar = rootView.findViewById(R.id.bar_volume);
        mCurrentMediaTimeView = rootView.findViewById(R.id.current_media_time);
        mMediaDurationView = rootView.findViewById(R.id.media_duration);

        mPlayPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayerController.getPlayerState() == MediaPlayerController.PlayerState.PLAYING)
                    mPlayerController.requestPause();
                else
                    mPlayerController.requestResume();

                mPlayPauseButton.setEnabled(false);
            }
        });

        mSkipPreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlayerController.requestSkipPrevious();
            }
        });

        mSkipNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlayerController.requestSkipNext();
            }
        });

        mCurrentMediaTimeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                setTimeText(mCurrentMediaTimeView, progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Empty
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mPlayerController.requestSeekTo(seekBar.getProgress());
            }
        });

        mRemoteVolumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Empty
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Empty
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mPlayerController.requestVolumeChange(seekBar.getProgress());
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        ClientFragment clientFragment = (ClientFragment) getParentFragment();

        if (clientFragment == null)
            throw new IllegalStateException("This fragment must have a ClientFragment as parent.");

        mPlayerController = clientFragment.getPlayerController();

        mPlayerController.setMediaPlayerObserver(mPlayerObserver);

        MediaPlayerController.PlayerState playerState = mPlayerController.getPlayerState();

        switch (playerState) {
            case IDLE:
                mPlayerObserver.onIdle();
                break;
            case LOADING:
                mPlayerObserver.onLoading();
                break;
            case PLAYING:
                mPlayerObserver.onPlaying();
                break;
            case PAUSED:
                mPlayerObserver.onPaused();
                break;
        }

        if (playerState != MediaPlayerController.PlayerState.IDLE) {
            mPlayerObserver.onMediaDurationChanged(mPlayerController.getMediaDuration());
            mPlayerObserver.onMediaTimeChanged(mPlayerController.getCurrentMediaTime());
        } else {
            mPlayerObserver.onMediaDurationChanged(0);
            mPlayerObserver.onMediaTimeChanged(0);
        }

        mPlayerObserver.onHasPreviousEntryChanged(mPlayerController.hasPreviousEntry());
        mPlayerObserver.onHasNextEntryChanged(mPlayerController.hasNextEntry());
        mPlayerObserver.onRemoteVolumeChanged(mPlayerController.getRemoteVolume());
    }

    @Override
    public void onPause() {
        super.onPause();

        mPlayerController.setMediaPlayerObserver(null);
    }

    private void setTimeText(TextView textView, int timeSeconds) {
        int hours, minutes, seconds;

        hours = timeSeconds / 3600;
        timeSeconds -= hours * 3600;
        minutes = timeSeconds / 60;
        seconds = timeSeconds - minutes * 60;

        if (hours == 0)
            textView.setText(String.format(Locale.US, "%02d:%02d", minutes, seconds));
        else
            textView.setText(String.format(Locale.US, "%d:%02d:%02d", hours, minutes, seconds));
    }

    private final MediaPlayerController.MediaPlayerObserver mPlayerObserver = new MediaPlayerController.MediaPlayerObserver() {
        @Override
        public void onIdle() {
            mLoadingBar.setVisibility(View.GONE);
            mPlayPauseButton.setEnabled(false);
            mPlayPauseButton.setImageDrawable(mPlayDrawable);
            mPlayPauseButton.setVisibility(View.VISIBLE);
            mCurrentMediaTimeBar.setProgress(0);
            mCurrentMediaTimeBar.setEnabled(false);
        }

        @Override
        public void onLoading() {
            mPlayPauseButton.setEnabled(false);
            mPlayPauseButton.setVisibility(View.INVISIBLE);
            mLoadingBar.setVisibility(View.VISIBLE);
            mCurrentMediaTimeBar.setEnabled(false);
        }

        @Override
        public void onPlaying() {
            mLoadingBar.setVisibility(View.GONE);
            mPlayPauseButton.setImageDrawable(mPauseDrawable);
            mPlayPauseButton.setEnabled(true);
            mPlayPauseButton.setVisibility(View.VISIBLE);
            mCurrentMediaTimeBar.setEnabled(true);
        }

        @Override
        public void onPaused() {
            mLoadingBar.setVisibility(View.GONE);
            mPlayPauseButton.setImageDrawable(mPlayDrawable);
            mPlayPauseButton.setEnabled(true);
            mPlayPauseButton.setVisibility(View.VISIBLE);
            mCurrentMediaTimeBar.setEnabled(true);
        }

        @Override
        public void onMediaTimeChanged(long currentMediaTime) {
            mCurrentMediaTimeBar.setProgress((int) (currentMediaTime / 1000));
        }

        @Override
        public void onMediaDurationChanged(long duration) {
            int durationSeconds = (int) (duration / 1000);

            mCurrentMediaTimeBar.setMax(durationSeconds);
            setTimeText(mMediaDurationView, durationSeconds);
        }

        @Override
        public void onHasNextEntryChanged(boolean hasNextEntry) {
            mSkipNextButton.setEnabled(hasNextEntry);
        }

        @Override
        public void onHasPreviousEntryChanged(boolean hasPreviousEntry) {
            mSkipPreviousButton.setEnabled(hasPreviousEntry);
        }

        @Override
        public void onRemoteVolumeChanged(int volume) {
            mRemoteVolumeBar.setProgress(volume);
        }
    };

    public static PlayerControllerFragment newInstance() {
        return new PlayerControllerFragment();
    }
}
