package com.taltenbach.mediacaster.ui;

import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.client.ClientPlaylistController;
import com.taltenbach.mediacaster.model.Favorites;
import com.taltenbach.mediacaster.util.ObservableValue;

public final class FavoriteListAdapter
        extends MediaListAdapter<MediaListAdapter.ViewHolder, MediaListAdapter.ViewHolder, MediaListAdapter.ViewHolder>
{
	private final Favorites mFavorites;
	private final FavoriteItemPopupMenu mItemPopupMenu;

    public FavoriteListAdapter(@NonNull FavoritesFragment favoriteFragment, @NonNull Favorites favorites, @NonNull ObservableValue<ClientPlaylistController> playlistController) {
        super(favorites, R.layout.item_favorite_media, R.layout.item_favorite_media);
		
		mFavorites = favorites;
		mItemPopupMenu = new FavoriteItemPopupMenu(favoriteFragment, playlistController);
    }

    @Override
    protected ViewHolder newPlatformMediaViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    protected ViewHolder newLocalMediaViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }
	
	@Override
	protected void showItemPopupMenu(int position, @NonNull View itemView, float x, float y) {
		mItemPopupMenu.showAt(itemView, x, y,  mFavorites.getFavorite(position), position);
	}
}
