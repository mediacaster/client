package com.taltenbach.mediacaster.ui;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DrawFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.taltenbach.mediacaster.R;

public abstract class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback {
    private final Drawable mBackground;
    private final Drawable mDeleteIcon;
    private final int mDeleteIconMarginSide;

    public SwipeToDeleteCallback(Context context, int swipeDirs) {
        super(0, swipeDirs);

        mBackground = new ColorDrawable(Color.rgb(0xD3, 0x2F, 0x2F));
        mDeleteIcon = ContextCompat.getDrawable(context, R.drawable.ic_delete_white_48);

        mDeleteIconMarginSide = context.getResources()
                .getDimensionPixelOffset(R.dimen.item_swipe_delete_icon_margin_side);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if (dX == 0) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            return;
        }

        View itemView = viewHolder.itemView;
        int deleteIconTop = (itemView.getTop() + itemView.getBottom() - mDeleteIcon.getIntrinsicHeight()) / 2;

        if (dX > 0) {
            mBackground.setBounds(0, itemView.getTop(), itemView.getLeft() + (int) dX, itemView.getBottom());

            int deleteIconLeft = itemView.getLeft() + mDeleteIconMarginSide;

            mDeleteIcon.setBounds(deleteIconLeft, deleteIconTop,
                    deleteIconLeft + mDeleteIcon.getIntrinsicWidth(),
                    deleteIconTop + mDeleteIcon.getIntrinsicHeight());
        } else if (dX < 0) {
            mBackground.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());

            int deleteIconRight = itemView.getRight() - mDeleteIconMarginSide;

            mDeleteIcon.setBounds(deleteIconRight - mDeleteIcon.getIntrinsicWidth(),
                    deleteIconTop, deleteIconRight,
                    deleteIconTop + mDeleteIcon.getIntrinsicHeight());
        }

        mBackground.draw(c);
        mDeleteIcon.draw(c);

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }
}
