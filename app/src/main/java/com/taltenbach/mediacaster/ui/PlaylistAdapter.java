package com.taltenbach.mediacaster.ui;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.client.ClientPlaylistController;
import com.taltenbach.mediacaster.model.Favorites;
import com.taltenbach.mediacaster.model.LocalMedia;
import com.taltenbach.mediacaster.model.PlatformMedia;
import com.taltenbach.mediacaster.model.Playlist;
import com.taltenbach.mediacaster.model.PlaylistEntry;
import com.taltenbach.mediacaster.util.loading.ImageLoader;

import java.util.List;

public final class PlaylistAdapter
        extends MediaListAdapter<PlaylistAdapter.PlaylistViewHolder, PlaylistAdapter.PlatformMediaViewHolder,
            PlaylistAdapter.LocalMediaViewHolder>
{
    private final ClientPlaylistController mPlaylistController;
    private final Favorites mFavorites;
    private final RecyclerView.LayoutManager mLayoutManager;
	private final PlaylistItemPopupMenu mItemPopupMenu;

    private int mCurrentEntry;
	
	private final Playlist.CurrentEntryChangeObserver mCurrentEntryChangeObserver = new Playlist.CurrentEntryChangeObserver() {
            @Override
            public void onCurrentEntryChanged(int newEntryIndex, PlaylistEntry newCurrentEntry) {
                int oldCurrentEntry = mCurrentEntry;
				mCurrentEntry = newEntryIndex;
				
				Object currentEntryChangedPayload = new Object();
				
				if (oldCurrentEntry != Playlist.NO_INDEX)
                    notifyItemChanged(oldCurrentEntry, currentEntryChangedPayload);

				if (newEntryIndex != Playlist.NO_INDEX) {
					notifyItemChanged(newEntryIndex, currentEntryChangedPayload);
					mLayoutManager.scrollToPosition(newEntryIndex);    
				}
            }
        };

    static abstract class PlaylistViewHolder extends MediaListAdapter.ViewHolder {
        private final TextView mAdderNameView;

        PlaylistViewHolder(View itemView) {
            super(itemView);

            mAdderNameView = itemView.findViewById(R.id.adder_name);
        }

        TextView getAdderNameView() {
            return mAdderNameView;
        }
    }

    static final class PlatformMediaViewHolder extends PlaylistViewHolder {
        private final FavoriteButton mFavoriteButton;
        private final PlatformMedia.PlatformMediaObserver mMediaObserver;
        private PlatformMedia mMedia;

        PlatformMediaViewHolder(View itemView) {
            super(itemView);

            mFavoriteButton = itemView.findViewById(R.id.btn_favorite);

            mMediaObserver = new PlatformMedia.PlatformMediaObserver() {
                @Override
                public void onTitleChanged(String newTitle) {
                    getAdderNameView().setText(newTitle);
                }

                @Override
                public void onThumbnailChanged(ImageLoader newImage) {
                    newImage.into(getThumbnailView());
                }

                @Override
                public void onFavoriteChanged(boolean isFavorite) {
                    mFavoriteButton.setChecked(isFavorite);
                }
            };
        }

        // TODO: call observe(null) when PlaylistAdapter is destroyed --> otherwise ViewHolders are leaked !
        void observe(@Nullable PlatformMedia media) {
            if (mMedia != null)
                mMedia.removePlatformMediaObserver(mMediaObserver);

            mMedia = media;

            if (media != null)
                media.addPlatformMediaObserver(mMediaObserver);
        }
    }

    static final class LocalMediaViewHolder extends PlaylistViewHolder {
        private final Button mDownloadButton;

        LocalMediaViewHolder(View itemView) {
            super(itemView);

            mDownloadButton = itemView.findViewById(R.id.btn_download);
        }
    }

    public PlaylistAdapter(@NonNull PlaylistFragment playlistFragment, @NonNull ClientPlaylistController playlistController, @NonNull Favorites favorites, @NonNull final RecyclerView.LayoutManager layoutManager) {
        super(playlistController.getPlaylist(), R.layout.item_playlist_platform_media, R.layout.item_playlist_local_media);

        mPlaylistController = playlistController;
        mFavorites = favorites;
        mLayoutManager = layoutManager;
		mItemPopupMenu = new PlaylistItemPopupMenu(playlistFragment, playlistController);
		
        Playlist playlist = playlistController.getPlaylist();

        mCurrentEntry = playlist.getCurrentEntryIndex();

        if (mCurrentEntry != Playlist.NO_INDEX)
            layoutManager.scrollToPosition(mCurrentEntry);

        playlist.addCurrentEntryChangeObserver(mCurrentEntryChangeObserver);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        holder.itemView.setSelected(position == mCurrentEntry);

        PlaylistEntry entry = mPlaylistController.getPlaylist().getEntry(position);

        holder.mAdderNameView.setText(entry.getAdderName());
    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
            return;
        }

        holder.itemView.setSelected(position == mCurrentEntry);
    }

    @Override
    protected void displayPlatformMedia(@NonNull final PlatformMedia media, @NonNull final PlatformMediaViewHolder holder) {
        super.displayPlatformMedia(media, holder);

        holder.mFavoriteButton.setChecked(media.isFavorite());

        holder.mFavoriteButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    mFavorites.add(media);
                else
                    mFavorites.removeNow(media);
            }
        });

        holder.observe(media);
    }

    @Override
    protected void displayLocalMedia(@NonNull LocalMedia media, @NonNull LocalMediaViewHolder holder) {
        super.displayLocalMedia(media, holder);

        holder.mDownloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: implements download
            }
        });
    }

    @Override
    protected PlatformMediaViewHolder newPlatformMediaViewHolder(View itemView) {
        return new PlatformMediaViewHolder(itemView);
    }

    @Override
    protected LocalMediaViewHolder newLocalMediaViewHolder(View itemView) {
        return new LocalMediaViewHolder(itemView);
    }
	
	@Override
	protected void showItemPopupMenu(int position, @NonNull View itemView, float x, float y) {
		mItemPopupMenu.showAt(itemView, x, y, mPlaylistController.getPlaylist().getEntry(position), position);
	}

    @Override
    public void onViewRecycled(@NonNull PlaylistViewHolder holder) {
        super.onViewRecycled(holder);

        // TODO : check if this is called for *all* viewholders when recyclerview is destroyed
        if (holder instanceof PlatformMediaViewHolder)
            ((PlatformMediaViewHolder) holder).observe(null);
    }

    public void destroy() {
        mPlaylistController.getPlaylist().removeCurrentEntryChangeObserver(mCurrentEntryChangeObserver);

        // TODO : need to set mLayoutManager and mItemPopupMenu to null to enable gc ?

        /*for (int childCount = recyclerView.getChildCount(), i = 0; i < childCount; ++i) {
            final ViewHolder holder = recyclerView.getChildViewHolder(recyclerView.getChildAt(i));

        }*/
    }

    /*@Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        mPlaylistController.getPlaylist().setOnCurrentEntryChangedObserver(null);
    }*/
}
