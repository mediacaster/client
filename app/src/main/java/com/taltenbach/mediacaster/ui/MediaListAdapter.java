package com.taltenbach.mediacaster.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.taltenbach.mediacaster.model.ObservableMediaList;
import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.model.LocalMedia;
import com.taltenbach.mediacaster.model.Media;
import com.taltenbach.mediacaster.model.MediaVisitor;
import com.taltenbach.mediacaster.model.PlatformMedia;
import com.taltenbach.mediacaster.util.MutableInteger;

public abstract class MediaListAdapter<BaseViewHolderT extends MediaListAdapter.ViewHolder,
        PlatformMediaViewHolderT extends BaseViewHolderT,
        LocalMediaViewHolderT extends BaseViewHolderT> extends RecyclerView.Adapter<BaseViewHolderT>
{
    private static final int PLATFORM_MEDIA_TYPE = 0;
    private static final int LOCAL_MEDIA_TYPE = 1;

    private final ObservableMediaList mMediaList;
    private final int mPlatformMediaLayoutResId;
    private final int mLocalMediaLayoutResId;
    private final LastTouchRecorder mLastTouchRecorder;

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView mThumbnailView;
        private final ImageView mPlatformLogoView;
        private final TextView mTitleView;

        ViewHolder(View itemView) {
            super(itemView);

            mThumbnailView = itemView.findViewById(R.id.thumbnail);
            mPlatformLogoView = itemView.findViewById(R.id.platform_logo);
            mTitleView = itemView.findViewById(R.id.title);
        }

        ImageView getThumbnailView() {
            return mThumbnailView;
        }

        ImageView getPlatformLogoView() {
            return mPlatformLogoView;
        }

        TextView getTitleView() {
            return mTitleView;
        }

        public Context getContext() {
            return mThumbnailView.getContext();
        }
    }

    protected MediaListAdapter(@NonNull ObservableMediaList mediaList, @LayoutRes int platformMediaLayoutResId, @LayoutRes int localMediaLayoutResId) {
        mMediaList = mediaList;
        mPlatformMediaLayoutResId = platformMediaLayoutResId;
        mLocalMediaLayoutResId = localMediaLayoutResId;
        mLastTouchRecorder = new LastTouchRecorder();

        mMediaList.addListChangeObserver(new ObservableMediaList.ListChangeObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                notifyItemRangeInserted(positionStart, itemCount);
            }

            @Override
            public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
                for (int i = 0; i < itemCount; ++i)
                    notifyItemMoved(fromPosition + i, toPosition + i);
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                notifyItemRangeRemoved(positionStart, itemCount);
                //notifyItemRangeChanged(positionStart, getItemCount());
            }
        });
    }

    @Override
    public final int getItemViewType(int position) {
        final MutableInteger type = new MutableInteger();

        mMediaList.getMedia(position).acceptVisitor(new MediaVisitor() {
            @Override
            public void visit(PlatformMedia platformMedia) {
                type.setValue(PLATFORM_MEDIA_TYPE);
            }

            @Override
            public void visit(LocalMedia localMedia) {
                type.setValue(LOCAL_MEDIA_TYPE);
            }
        });

        return type.getValue();
    }

    @NonNull
    @Override
    public final BaseViewHolderT onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case PLATFORM_MEDIA_TYPE:
                return newPlatformMediaViewHolder(inflater.inflate(mPlatformMediaLayoutResId, parent, false));
            case LOCAL_MEDIA_TYPE:
                return newLocalMediaViewHolder(inflater.inflate(mLocalMediaLayoutResId, parent, false));
            default:
                throw new IllegalArgumentException("Invalid viewType value");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final BaseViewHolderT holder, int position) {
        View itemView = holder.itemView;

		itemView.setOnTouchListener(mLastTouchRecorder);

		itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showItemPopupMenu(holder);
            }
        });

		itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showItemPopupMenu(holder);
                return true;
            }
        });

        Media media = mMediaList.getMedia(position);

        holder.getTitleView().setText(media.getTitle());

        media.loadThumbnail().into(holder.getThumbnailView());

        media.acceptVisitor(new MediaVisitor() {
            @SuppressWarnings("unchecked")
            @Override
            public void visit(PlatformMedia platformMedia) {
                //Cast ensured in onCreateViewHolder()
                displayPlatformMedia(platformMedia, (PlatformMediaViewHolderT) holder);
            }

            @SuppressWarnings("unchecked")
            @Override
            public void visit(LocalMedia localMedia) {
                //Cast ensured in onCreateViewHolder()
                displayLocalMedia(localMedia, (LocalMediaViewHolderT) holder);
            }
        });
    }

    @Override
    public final int getItemCount() {
        return mMediaList.getMediaCount();
    }

    protected void displayPlatformMedia(@NonNull PlatformMedia media, @NonNull PlatformMediaViewHolderT holder) {
        holder.getPlatformLogoView().setImageResource(media.getPlatform().getLogoResId());
        holder.getPlatformLogoView().setVisibility(View.VISIBLE);
    }

    protected void displayLocalMedia(@NonNull LocalMedia media, @NonNull LocalMediaViewHolderT holder) {
        holder.getPlatformLogoView().setVisibility(View.GONE);
    }

    protected abstract PlatformMediaViewHolderT newPlatformMediaViewHolder(View itemView);

    protected abstract LocalMediaViewHolderT newLocalMediaViewHolder(View itemView);
	
	protected abstract void showItemPopupMenu(int position, @NonNull View itemView, float x, float y);

	private void showItemPopupMenu(@NonNull BaseViewHolderT viewHolder) {
	    showItemPopupMenu(viewHolder.getAdapterPosition(), viewHolder.itemView, mLastTouchRecorder.mLastTouchX, mLastTouchRecorder.mLastTouchY);
    }

    private static final class LastTouchRecorder implements View.OnTouchListener {
        private float mLastTouchX;
        private float mLastTouchY;

        @SuppressLint("ClickableViewAccessibility") // Returning false, so no need to call performClick() manually
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mLastTouchX = event.getX();
            mLastTouchY = event.getY();
            return false;
        }
    }
}
