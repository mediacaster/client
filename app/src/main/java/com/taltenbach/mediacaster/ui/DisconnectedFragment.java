package com.taltenbach.mediacaster.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.taltenbach.mediacaster.R;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.ui
 * Creator : Thomas ALTENBACH
 * Date : 26/07/2018
 */

public class DisconnectedFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_disconnected, container, false);

        Button connectButton = rootView.findViewById(R.id.btn_connect);

        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClientFragment clientFragment = (ClientFragment) getParentFragment();

                if (clientFragment == null)
                    throw new IllegalStateException("This fragment must have a ClientFragment as parent.");

                clientFragment.showConnectionDialog();
            }
        });

        return rootView;
    }

    public static DisconnectedFragment newInstance() {
        return new DisconnectedFragment();
    }
}
