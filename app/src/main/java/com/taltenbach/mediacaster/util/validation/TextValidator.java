package com.taltenbach.mediacaster.util.validation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.util
 * Creator : Thomas ALTENBACH
 * Date : 23/07/2018
 */

public final class TextValidator implements TextWatcher {
    private final EditText mEditText;
    private final CharSequence mErrorMessage;
    private final Validator<String> mValidator;

    private boolean mIsValid;
    private ValidationObserver mValidationObserver;

    private TextValidator(@NonNull EditText editText, @Nullable CharSequence errorMessage, @NonNull Validator<String> validator) {
        mEditText = editText;
        mErrorMessage = errorMessage;
        mIsValid = validator.isValid(editText.getText().toString());
        mValidator = validator;
    }

    /**
     * Checks if the current text of the EditText is valid and, if not, set EditText error.
     * Calling this method can be useful to display error when the user wants to proceed but
     * has forgotten to fill the EditText.
     *
     * @return true if the current text of the EditText is valid, false otherwise
     */
    public boolean validate() {
        String text = mEditText.getText().toString();
        boolean isValid = mValidator.isValid(text);

        if (isValid != mIsValid) {
            mIsValid = isValid;

            if (mValidationObserver != null)
                mValidationObserver.onValidationChanged(isValid);
        }

        if (!isValid)
            mEditText.setError(mErrorMessage);

        return isValid;
    }

    /**
     * Indicates whether the current text of the EditText is valid or not
     *
     * @return true if the current text of the EditText is valid, false otherwise
     */
    public boolean isValid() {
        return mIsValid;
    }

    public void setValidationObserver(ValidationObserver observer) {
        mValidationObserver = observer;
    }

    @Override
    final public void afterTextChanged(Editable s) {
        validate();
    }

    @Override
    final public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // Empty
    }

    @Override
    final public void onTextChanged(CharSequence s, int start, int before, int count) {
        // Empty
    }

    /**
     * Creates a new TextValidator that watches and responds to the text changes of the specified EditText
     * @param editText The EditText
     * @param errorMessage The error message to display in case of invalid text (set to null if you do not want to show an error message)
     * @param validator The validator used to validate the text
     * @return The newly created TextValidator
     */
    public static TextValidator watch(@NonNull EditText editText, @Nullable CharSequence errorMessage, @NonNull Validator<String> validator) {
        TextValidator textValidator = new TextValidator(editText, errorMessage, validator);
        editText.addTextChangedListener(textValidator);
        return textValidator;
    }

    /**
     * Creates a new TextValidator that watches and responds to the text changes of the specified EditText
     * @param editText The EditText
     * @param errorMessageResId The resource id of the error message to display in case of invalid text (set to 0 if you do not want to show an error message)
     * @param validator The validator used to validate the text
     * @return The newly created TextValidator
     */
    public static TextValidator watch(@NonNull EditText editText, @StringRes int errorMessageResId, @NonNull Validator<String> validator) {
        return watch(editText, editText.getResources().getString(errorMessageResId), validator);
    }
}
