package com.taltenbach.mediacaster.util.loading;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

import com.taltenbach.mediacaster.util.ResultCallback;

public abstract class ImageLoader {
    @DrawableRes
    private final int mDefaultImageResId;

    protected ImageLoader() {
        mDefaultImageResId = 0;
    }

    protected ImageLoader(@DrawableRes int defaultImageResId) {
        mDefaultImageResId = defaultImageResId;
    }

    public abstract void into(ImageView imageView);

    public abstract void then(Context context, ResultCallback<Bitmap> onLoaded);

    protected int getDefaultImageResId() {
        return mDefaultImageResId;
    }

    protected Bitmap getDefaultImage(Context context) {
        return BitmapFactory.decodeResource(context.getResources(), mDefaultImageResId);
    }
}
