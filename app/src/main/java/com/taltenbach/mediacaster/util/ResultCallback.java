package com.taltenbach.mediacaster.util;

public interface ResultCallback<T> {
    void onResult(T result);
}
