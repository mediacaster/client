package com.taltenbach.mediacaster.util;

public final class MutableInteger {
    private int mValue;

    public MutableInteger() {
        this(0);
    }

    public MutableInteger(int value) {
        mValue = value;
    }

    public int getValue() {
        return mValue;
    }

    public void setValue(int value) {
        mValue = value;
    }
}
