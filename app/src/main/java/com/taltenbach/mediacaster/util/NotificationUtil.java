package com.taltenbach.mediacaster.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.annotation.StringRes;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.util
 * Creator : Thomas ALTENBACH
 * Date : 16/08/2018
 */

public final class NotificationUtil {
    private NotificationUtil() {
        // Empty
    }

    @NonNull
    public static NotificationManager getManager(@NonNull Context c) {
        //noinspection ConstantConditions
        return (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public static void notify(@NonNull Context c, int id, Notification notif) {
        getManager(c).notify(id, notif);
    }

    public static void cancel(@NonNull Context c, int id) {
        getManager(c).cancel(id);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void createChannel(@NonNull Context c, @NonNull String id, @StringRes int name, @StringRes int desc, int importance) {
        NotificationChannel channel = new NotificationChannel(id, c.getText(name), importance);
        channel.setDescription(c.getString(desc));

        getManager(c).createNotificationChannel(channel);
    }
}
