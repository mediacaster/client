package com.taltenbach.mediacaster.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;

import com.taltenbach.mediacaster.client.PlaylistClientService;
import com.taltenbach.mediacaster.ui.ClientFragment;

public class PlaylistServiceRunningChecker extends BroadcastReceiver {
    private boolean mIsServiceRunning;

    private PlaylistServiceRunningChecker() {
        mIsServiceRunning = false;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        mIsServiceRunning = true;
    }

    public static boolean checkRunning(@NonNull Context context) {
        PlaylistServiceRunningChecker checker = new PlaylistServiceRunningChecker();
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);

        broadcastManager.registerReceiver(checker, new IntentFilter(PlaylistClientService.ACTION_ECHO));
        broadcastManager.sendBroadcastSync(new Intent(PlaylistClientService.ACTION_REQUEST_ECHO));
        broadcastManager.unregisterReceiver(checker);

        return checker.mIsServiceRunning;
    }
}
