package com.taltenbach.mediacaster.util.loading;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.util.Log;
import android.util.LruCache;
import android.widget.ImageView;

import com.taltenbach.mediacaster.util.ResultCallback;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.UnknownHostException;

public class UrlImageLoader extends ImageLoader {
    private static final int VM_MAX_MEMORY = (int) (Runtime.getRuntime().maxMemory() / 1024);
    private static final int IMAGE_CACHE_SIZE = VM_MAX_MEMORY / 8;

    private static final LruCache<String, Bitmap> sImageCache
            = new LruCache<String, Bitmap>(IMAGE_CACHE_SIZE) {
                @Override
                protected int sizeOf(String key, Bitmap bitmap) {
                    return bitmap.getByteCount() / 1024;
                }
            };

    private final String mImageUrl;

    public UrlImageLoader(String imageUrl) {
        super();

        mImageUrl = imageUrl;
    }

    public UrlImageLoader(String imageUrl, @DrawableRes int defaultImageResId) {
        super(defaultImageResId);

        mImageUrl = imageUrl;
    }

    @Override
    public void into(ImageView imageView) {
        Bitmap cachedBitmap = sImageCache.get(mImageUrl);

        if (cachedBitmap != null) {
            imageView.setImageBitmap(cachedBitmap);
            return;
        }

        imageView.setImageResource(getDefaultImageResId());

        final WeakReference<ImageView> imageViewRef = new WeakReference<>(imageView);

        new ImageLoaderTask(new ResultCallback<Bitmap>() {
            @Override
            public void onResult(Bitmap result) {
                ImageView view = imageViewRef.get();

                if (view != null && result != null)
                    view.setImageBitmap(result);
            }
        }).execute(mImageUrl);
    }

    @Override
    public void then(Context context, final ResultCallback<Bitmap> onLoaded) {
        Bitmap cachedBitmap = sImageCache.get(mImageUrl);

        if (cachedBitmap != null) {
            onLoaded.onResult(cachedBitmap);
            return;
        }

        final WeakReference<Context> contextRef = new WeakReference<>(context);

        new ImageLoaderTask(new ResultCallback<Bitmap>() {
            @Override
            public void onResult(Bitmap result) {
                if (result == null && contextRef.get() != null)
                    onLoaded.onResult(getDefaultImage(contextRef.get()));
                else
                    onLoaded.onResult(result);
            }
        }).execute(mImageUrl);
    }

    private static class ImageLoaderTask extends AsyncTask<String, Void, Bitmap> {
        private final ResultCallback<Bitmap> mOnLoaded;

        ImageLoaderTask(ResultCallback<Bitmap> onLoaded) {
            mOnLoaded = onLoaded;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            if (urls.length == 0)
                return null;

            String url = urls[0];
            Bitmap image = null;

            try {
                InputStream in = new URL(url).openStream();
                image = BitmapFactory.decodeStream(in);

                sImageCache.put(url, image);
            } catch (IOException e) {
                Log.e("ImageLoaderTask", e.getMessage());
                e.printStackTrace();
            }

            return image;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            mOnLoaded.onResult(bitmap);
        }
    }
}
