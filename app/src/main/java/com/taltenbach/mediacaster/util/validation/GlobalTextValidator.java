package com.taltenbach.mediacaster.util.validation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.util
 * Creator : Thomas ALTENBACH
 * Date : 24/07/2018
 */

public class GlobalTextValidator {
    private final List<TextValidator> mValidators;

    private int mValidFields;
    private ValidationObserver mValidationObserver;

    private ValidationObserver mEditTextValidationObserver = new ValidationObserver() {
        @Override
        public void onValidationChanged(boolean isValid) {
            if (isValid) {
                ++mValidFields;

                if (mValidationObserver != null && mValidFields == mValidators.size())
                    mValidationObserver.onValidationChanged(true);
            } else {
                --mValidFields;

                if (mValidationObserver != null && mValidFields == mValidators.size() - 1)
                    mValidationObserver.onValidationChanged(false);
            }
        }
    };

    public GlobalTextValidator() {
        mValidators = new ArrayList<>();
    }

    public GlobalTextValidator addField(@NonNull EditText editText, @Nullable CharSequence errorMessage, @NonNull Validator<String> validator) {
        TextValidator textValidator = TextValidator.watch(editText, errorMessage, validator);
        addField(textValidator);
        return this;
    }

    public GlobalTextValidator addField(@NonNull EditText editText, @StringRes int errorMessageResId, @NonNull Validator<String> validator) {
        TextValidator textValidator = TextValidator.watch(editText, errorMessageResId, validator);
        addField(textValidator);
        return this;
    }

    private void addField(TextValidator textValidator) {
        textValidator.setValidationObserver(mEditTextValidationObserver);

        if (textValidator.isValid())
            ++mValidFields;

        mValidators.add(textValidator);
    }

    public boolean validate() {
        boolean isValid = true;

        for (TextValidator validator : mValidators)
            isValid &= validator.validate();

        return isValid;
    }

    public boolean isValid() {
        return mValidFields == mValidators.size();
    }

    public void setValidationObserver(ValidationObserver observer) {
        mValidationObserver = observer;
    }
}
