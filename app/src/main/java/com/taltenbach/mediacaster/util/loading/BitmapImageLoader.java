package com.taltenbach.mediacaster.util.loading;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

import com.taltenbach.mediacaster.util.ResultCallback;

public class BitmapImageLoader extends ImageLoader {
    private final Bitmap mBitmap;

    public BitmapImageLoader(Bitmap bitmap) {
        super();

        mBitmap = bitmap;
    }

    public BitmapImageLoader(Bitmap bitmap, @DrawableRes int defaultImageResId) {
        super(defaultImageResId);

        mBitmap = bitmap;
    }

    @Override
    public void into(ImageView imageView) {
        if (mBitmap != null)
            imageView.setImageBitmap(mBitmap);
        else
            imageView.setImageResource(getDefaultImageResId());
    }

    @Override
    public void then(Context context, ResultCallback<Bitmap> onLoaded) {
        onLoaded.onResult(mBitmap != null ? mBitmap : getDefaultImage(context));
    }
}
