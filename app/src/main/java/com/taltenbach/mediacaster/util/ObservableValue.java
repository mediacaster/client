package com.taltenbach.mediacaster.util;

import android.support.annotation.NonNull;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.util
 * Creator : Thomas ALTENBACH
 * Date : 15/08/2018
 */

public interface ObservableValue<T> {
    T getValue();
	
	void addValueChangeObserver(@NonNull ValueChangeObserver<T> observer);
	
	void removeValueChangeObserver(ValueChangeObserver<T> observer);
	
	interface ValueChangeObserver<T> {
		void onValueChanged(T newValue);
	}
}
