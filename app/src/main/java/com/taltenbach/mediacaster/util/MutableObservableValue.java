package com.taltenbach.mediacaster.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.HashSet;
import java.util.Set;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.util
 * Creator : Thomas ALTENBACH
 * Date : 15/08/2018
 */

public class MutableObservableValue<T> implements ObservableValue<T> {
	private T mValue;
	
	private final Set<ValueChangeObserver<T>> mObservers;
	
	public MutableObservableValue() {
		mObservers = new HashSet<>();
	}
	
	public MutableObservableValue(@Nullable T value) {
		this();
		
		mValue = value;
	}
	
	public T getValue() {
		return mValue;
	}
	
	public void setValue(T value) {
		if (value == mValue || (value != null && value.equals(mValue)))
			return;
		
		mValue = value;
		
		for (ValueChangeObserver<T> o : mObservers)
			o.onValueChanged(value);
	}
	
	public void addValueChangeObserver(@NonNull ValueChangeObserver<T> observer) {
		mObservers.add(observer);
	}
	
	public void removeValueChangeObserver(ValueChangeObserver<T> observer) {
		mObservers.remove(observer);
	}
}
