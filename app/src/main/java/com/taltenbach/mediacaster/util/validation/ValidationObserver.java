package com.taltenbach.mediacaster.util.validation;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.util
 * Creator : Thomas ALTENBACH
 * Date : 24/07/2018
 */

public interface ValidationObserver {
    void onValidationChanged(boolean isValid);
}
