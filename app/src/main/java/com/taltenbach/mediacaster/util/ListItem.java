package com.taltenbach.mediacaster.util;

public final class ListItem<T> {
    private final int mPos;
    private final T mValue;

    public ListItem(int pos, T value) {
        mPos = pos;
        mValue = value;
    }

    public int getPos() {
        return mPos;
    }

    public T getValue() {
        return mValue;
    }
}
