package com.taltenbach.mediacaster.model;

import com.taltenbach.mediacaster.client.ClientInputStream;

import java.io.IOException;

abstract class BaseMedia implements Media {
    private final MediaType mMediaType;
    private String mTitle;

    BaseMedia(MediaType mediaType, String title) {
        mMediaType = mediaType;
        mTitle = title;
    }

    public MediaType getMediaType() {
        return mMediaType;
    }

    public String getTitle() {
        return mTitle;
    }

    void setTitle(String newTitle) {
        mTitle = newTitle;
    }

    abstract static class Builder<T extends BaseMedia> {
        private final MediaType mMediaType;
        private final String mTitle;

        Builder(ClientInputStream in) throws IOException {
            mMediaType = MediaType.fromOrdinal(in.readShort());
            mTitle = in.readString();
        }

        MediaType getMediaType() {
            return mMediaType;
        }

        String getTitle() {
            return mTitle;
        }

        abstract T build();
    }
}
