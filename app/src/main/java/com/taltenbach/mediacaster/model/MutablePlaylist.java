package com.taltenbach.mediacaster.model;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class MutablePlaylist implements Playlist {
    private final ObservableList<PlaylistEntry> mEntries;
    private int mCurrentEntryIndex;

    private final ListObservers<PlaylistEntry> mPlaylistObservers;
    private final Set<CurrentEntryChangeObserver> mCurrentEntryChangeObservers;

    public MutablePlaylist() {
        this(null, NO_INDEX);
    }

    /**
     * Create a new playlist
     * @param initialEntries Initial entries in the playlist (can be set to null for an empty playlist)
     * @param currentEntryIndex Current playlist entry index (Playlist.NO_INDEX if none)
     */
    public MutablePlaylist(@Nullable PlaylistEntry[] initialEntries, int currentEntryIndex) {
        mEntries = new ObservableArrayList<>();

        if (initialEntries != null)
            mEntries.addAll(Arrays.asList(initialEntries));

        if (currentEntryIndex != Playlist.NO_INDEX && currentEntryIndex >= mEntries.size())
            throw new IndexOutOfBoundsException("The current entry index must be Playlist.NO_INDEX or a valid index for the initialEntries array");

        mCurrentEntryIndex = currentEntryIndex;

        mPlaylistObservers = new ListObservers<>();
		mCurrentEntryChangeObservers = new HashSet<>();
        mEntries.addOnListChangedCallback(mPlaylistObservers);
    }

    public void enqueue(PlaylistEntry entry) {
        mEntries.add(entry);
    }

    public void insert(int pos, PlaylistEntry entry) {
        mEntries.add(pos, entry);
    }

    public PlaylistEntry remove(int pos) {
        if (pos == mCurrentEntryIndex)
            setCurrentEntryIndex(NO_INDEX);

        return mEntries.remove(pos);
    }
	
	public int remove(@NonNull PlaylistEntry entry, int startSearchIndex) {
		int pos = getEntryIndex(entry.getEntryId(), startSearchIndex);
		
		if (pos >= 0)
			remove(pos);
		
		return pos;
	}

    public PlaylistEntry remove(long entryId, int startSearchIndex) {
        int pos = getEntryIndex(entryId, startSearchIndex);

        if (pos < 0)
            return null;

        return remove(pos);
    }

    public void moveUp(int pos) {
        if (pos == 0)
            return;

        moveEntry(pos, pos - 1);
    }

    public void moveUp(long entryId, int startSearchIndex) {
        int pos = getEntryIndex(entryId, startSearchIndex);

        if (pos < 0)
            return;

        moveUp(pos);
    }

    public void moveDown(int pos) {
        if (pos == mEntries.size() - 1)
            return;

        moveEntry(pos, pos + 1);
    }

    public void moveDown(long entryId, int startSearchIndex) {
        int pos = getEntryIndex(entryId, startSearchIndex);

        if (pos < 0)
            return;

        moveDown(pos);
    }

    private void moveEntry(int from, int to) {
        Collections.swap(mEntries, from, to);

        // ObservableArrayList only called onChanged on the moved items, not onItemRangeMoved.
        // We must then call it manually.
        mPlaylistObservers.onItemRangeMoved(mEntries, from, to, 1);
    }

	@Nullable
    public PlaylistEntry setCurrentEntryIndex(int currentEntryIndex) {
		boolean hasCurrentEntry = currentEntryIndex != NO_INDEX;
		
        if (hasCurrentEntry && (currentEntryIndex < 0 || currentEntryIndex >= mEntries.size()))
            throw new IndexOutOfBoundsException();

        mCurrentEntryIndex = currentEntryIndex;

		PlaylistEntry newCurrentEntry = hasCurrentEntry ? mEntries.get(currentEntryIndex) : null;
		
        for (CurrentEntryChangeObserver o : mCurrentEntryChangeObservers)
            o.onCurrentEntryChanged(currentEntryIndex, newCurrentEntry);

        return newCurrentEntry;
    }

    @Nullable
    public PlaylistEntry setCurrentEntry(long entryId, int startSearchIndex) {
        int pos = getEntryIndex(entryId, startSearchIndex);

        if (pos < 0)
            return null;

        return setCurrentEntryIndex(pos);
    }

    public PlaylistEntry getCurrentEntry() {
        if (mCurrentEntryIndex == NO_INDEX)
            return null;

        return mEntries.get(mCurrentEntryIndex);
    }

    @Override
    public PlaylistEntry getEntry(int pos) {
        return mEntries.get(pos);
    }

    @Override
    public int getCurrentEntryIndex() {
        return mCurrentEntryIndex;
    }

    @Override
    public boolean hasNextEntry() {
        return mCurrentEntryIndex != NO_INDEX && mCurrentEntryIndex != mEntries.size() - 1;
    }

    @Override
    public boolean hasPreviousEntry() {
        return mEntries.size() != 0 && mCurrentEntryIndex != 0;
    }

    @Override
    public void addCurrentEntryChangeObserver(@NonNull CurrentEntryChangeObserver observer) {
        mCurrentEntryChangeObservers.add(observer);
    }
	
	@Override
    public void removeCurrentEntryChangeObserver(CurrentEntryChangeObserver observer) {
        mCurrentEntryChangeObservers.remove(observer);
    }
	
	@Override
    public void addListChangeObserver(@NonNull ListChangeObserver observer) {
        mPlaylistObservers.addListChangeObserver(observer);
    }

    @Override
    public void removeListChangeObserver(ListChangeObserver observer) {
        mPlaylistObservers.removeListChangeObserver(observer);
    }

    @Override
    public Media getMedia(int pos) {
        return mEntries.get(pos).getMedia();
    }

    @Override
    public int getMediaCount() {
        return mEntries.size();
    }

    /**
     * Returns the index of the entry having the specified ID in the playlist
     * @param entryId The ID of the entry to search
     * @param startSearchIndex The index in the list of entries where to start the search
     * @return The index of the entry if the specified entry ID exists in the playlist (positive or zero integer),
     *         a strictly negative integer otherwise
     *
     * The search start from the specified index and continues on both sides of the index until
     * finding the entry having the given ID or having traversed the whole playlist.
     */
    private int getEntryIndex(long entryId, int startSearchIndex) {
        int i;
        int count = mEntries.size();

        if (count == 0)
            return -1;

        if (startSearchIndex < 0)
            i = 0;
        else if (startSearchIndex >= count)
            i = count - 1;
        else
            i = startSearchIndex;

        if (mEntries.get(i).getEntryId() == entryId)
            return i;

        int j = i;

        do {
            --i;
            ++j;
        } while (i >= 0 && j < count
                && mEntries.get(i).getEntryId() != entryId
                && mEntries.get(j).getEntryId() != entryId);

        if (i >= 0 && mEntries.get(i).getEntryId() == entryId)
            return i; // found

        if (j < count && mEntries.get(j).getEntryId() == entryId)
            return j; // found

        // not found yet : at least one bound of the entry list has been reached before finding the entry
        if (i >= 0) {
            do {
                --i;
            } while (i >= 0 && mEntries.get(i).getEntryId() != entryId);

            return i; // if not found, i == -1
        } else if (j < count) {
            do {
                ++j;
            } while (j < count && mEntries.get(j).getEntryId() != entryId);

            return j < count ? j : -1;
        }

        return -1;
    }
}
