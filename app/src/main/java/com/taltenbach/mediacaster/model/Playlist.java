package com.taltenbach.mediacaster.model;

import android.support.annotation.NonNull;

public interface Playlist extends ObservableMediaList {
    int NO_INDEX = -1;

    PlaylistEntry getEntry(int pos);

    /**
     * Returns the current entry index or NO_INDEX if there is no current entry.
     * @return The current entry index or NO_INDEX.
     */
    int getCurrentEntryIndex();

    /**
     * Returns the current entry or null if there is no current entry.
     * @return The current entry or null.
     */
    PlaylistEntry getCurrentEntry();

    boolean hasNextEntry();

    boolean hasPreviousEntry();

    void addCurrentEntryChangeObserver(@NonNull CurrentEntryChangeObserver observer);
	
	void removeCurrentEntryChangeObserver(CurrentEntryChangeObserver observer);

    interface CurrentEntryChangeObserver {
        void onCurrentEntryChanged(int newEntryIndex, PlaylistEntry newCurrentEntry);
    }
}
