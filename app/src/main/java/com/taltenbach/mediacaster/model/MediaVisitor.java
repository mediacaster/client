package com.taltenbach.mediacaster.model;

public interface MediaVisitor {
    void visit(PlatformMedia platformMedia);
    void visit(LocalMedia localMedia);
}
