package com.taltenbach.mediacaster.model;

import android.support.annotation.DrawableRes;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.util.loading.ImageLoader;

public interface Media {
    enum MediaType {
        AUDIO (R.drawable.default_audio_thumbnail),
        VIDEO (R.drawable.default_video_thumbnail);

        private static final MediaType[] VALUES = MediaType.values();

        @DrawableRes
        private final int mDefaultThumbnailResId;

        MediaType(@DrawableRes int defaultThumbnailResId) {
            mDefaultThumbnailResId = defaultThumbnailResId;
        }

        @DrawableRes
        int getDefaultThumbnailResId() {
            return mDefaultThumbnailResId;
        }

        public static MediaType fromOrdinal(int ordinal) {
            return VALUES[ordinal];
        }
    }

    MediaType getMediaType();

    String getTitle();

    ImageLoader loadThumbnail();

    void acceptVisitor(MediaVisitor visitor);
}
