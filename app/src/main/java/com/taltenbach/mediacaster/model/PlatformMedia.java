package com.taltenbach.mediacaster.model;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.util.SparseArray;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.client.ClientInputStream;
import com.taltenbach.mediacaster.db.FavoriteEntity;
import com.taltenbach.mediacaster.util.loading.ImageLoader;
import com.taltenbach.mediacaster.util.loading.UrlImageLoader;
import com.taltenbach.mediacaster.util.WeakValueHashMap;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class PlatformMedia extends BaseMedia {
    private static final Map<PlatformMediaId, PlatformMedia> sPlatformMediaInstances = new WeakValueHashMap<>();

    private final PlatformMediaId mId;
    private String mThumbnailUrl;
    private boolean mIsFavorite;

    private final Set<PlatformMediaObserver> mObservers;

    public enum Platform {
        YOUTUBE(0, R.drawable.logo_youtube_36),
        VIMEO(1, R.drawable.logo_vimeo_36);

        private static final SparseArray<Platform> VALUES = new SparseArray<>();

        static {
            for (Platform p : Platform.values())
                VALUES.append(p.mId, p);
        }

        private final short mId;

        @DrawableRes
        private final int mLogoResId;

        Platform(int id, @DrawableRes int logoResId) {
            mId = (short) id;
            mLogoResId = logoResId;
        }

        public short getId() {
            return mId;
        }

        @DrawableRes
        public int getLogoResId() {
            return mLogoResId;
        }

        public static Platform fromId(short id) {
            return VALUES.get(id);
        }
    }

    private PlatformMedia(MediaType mediaType, String title, @NonNull PlatformMediaId id, String thumbnailUrl, boolean isFavorite) {
        super(mediaType, title);

        mId = id;
        mThumbnailUrl = thumbnailUrl;
        mIsFavorite = isFavorite;
        mObservers = new HashSet<>();
    }

    public Platform getPlatform() {
        return mId.mPlatform;
    }

    public String getPlatformKey() {
        return mId.mPlatformKey;
    }

    public boolean isFavorite() {
        return mIsFavorite;
    }

    public String getThumbnailUrl() {
        return mThumbnailUrl;
    }

    void setFavorite(boolean isFavorite) {
        if (isFavorite == mIsFavorite)
            return;

        mIsFavorite = isFavorite;

        for (PlatformMediaObserver o : mObservers)
            o.onFavoriteChanged(isFavorite);
    }

    private void setThumbnailUrl(String newUrl) {
        /*if (newUrl.equals(mThumbnailUrl))
            return;*/

        mThumbnailUrl = newUrl;

        ImageLoader newImage = loadThumbnail();

        for (PlatformMediaObserver o : mObservers)
            o.onThumbnailChanged(newImage);
    }

    @Override
    void setTitle(String newTitle) {
        /*if (newTitle.equals(getTitle()))
            return;*/

        super.setTitle(newTitle);

        for (PlatformMediaObserver o : mObservers)
            o.onTitleChanged(newTitle);
    }

    @Override
    public ImageLoader loadThumbnail() {
        return new UrlImageLoader(mThumbnailUrl, getMediaType().getDefaultThumbnailResId());
    }

    @Override
    public void acceptVisitor(MediaVisitor visitor) {
        visitor.visit(this);
    }

    public void addPlatformMediaObserver(PlatformMediaObserver observer) {
        mObservers.add(observer);
    }

    public void removePlatformMediaObserver(PlatformMediaObserver observer) {
        mObservers.remove(observer);
    }

    @Override
    public boolean equals(Object obj) {
        // Able to compare by reference because the getInstance methods ensure there is only one media instance per platform media ID
        return obj == this;
    }

    private static PlatformMedia getInstance(MediaType mediaType, String title, Platform platform,
                                                    String platformKey, String thumbnailUrl, boolean isFavorite)
    {
        PlatformMediaId id = new PlatformMediaId(platform, platformKey);
        PlatformMedia instance = sPlatformMediaInstances.get(id);

        if (instance == null) {
            instance = new PlatformMedia(mediaType, title, id, thumbnailUrl, isFavorite);
            sPlatformMediaInstances.put(id, instance);
        }

        return instance;
    }

    private static PlatformMedia getInstance(MediaType mediaType, String title, Platform platform,
                                            String platformKey, String thumbnailUrl)
    {
        return getInstance(mediaType, title, platform, platformKey, thumbnailUrl, false);
    }

    static PlatformMedia getInstance(FavoriteEntity favorite) {
        return getInstance(favorite.getMediaType(), favorite.getTitle(), favorite.getPlatform(),
                favorite.getPlatformKey(), favorite.getThumbnailUrl(), true);
    }

    public static boolean isValidMediaUrl(@NonNull String url) {
        return url.matches("^((https?://)?((((www.)?|(m.)?)youtube.com)|(youtu.be))/((watch\\?v=)|(v/)|(embed/))?|(\\\\s)?(https?://)?vimeo.com/)\\w*");
    }

    static final class Builder extends BaseMedia.Builder<PlatformMedia> {
        private final Platform mPlatform;
        private final String mPlatformKey;
        private final String mThumbnailUrl;

        Builder(ClientInputStream in) throws IOException {
            super(in);

            mPlatform = Platform.fromId(in.readShort());
            mPlatformKey = in.readString();
            mThumbnailUrl = in.readString();
        }

        /**
         * Returns the PlatformMedia corresponding to the information provided to this builder.
         * Note that this method *must* be called in the main thread.
         *
         * @return The PlatformMedia corresponding to the information provided to this builder.
         */
        @Override
        PlatformMedia build() {
            String title = getTitle();
            PlatformMedia media = getInstance(getMediaType(), title, mPlatform, mPlatformKey, mThumbnailUrl);

            // Update title if an old one is currently used
            if (!media.getTitle().equals(title))
                media.setTitle(title);

            // Update thumbnail url if an old one is currently used
            if (!media.getThumbnailUrl().equals(mThumbnailUrl))
                media.setThumbnailUrl(mThumbnailUrl);

            return media;
        }
    }

    public interface PlatformMediaObserver {
        void onTitleChanged(String newTitle);

        void onThumbnailChanged(ImageLoader newImage);

        void onFavoriteChanged(boolean isFavorite);
    }

    static final class PlatformMediaId {
        private final Platform mPlatform;
        private final String mPlatformKey;

        PlatformMediaId(Platform platform, @NonNull String platformKey) {
            mPlatform = platform;
            mPlatformKey = platformKey;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof PlatformMediaId))
                return false;

            PlatformMediaId other = (PlatformMediaId) obj;

            return mPlatform == other.mPlatform
                    && mPlatformKey.equals(other.mPlatformKey);
        }

        @Override
        public int hashCode() {
            int result = 1;
            result = 31 * result + mPlatform.hashCode();
            result = 31 * result + mPlatformKey.hashCode();

            return result;
        }
    }
}
