package com.taltenbach.mediacaster.model;

import android.content.Context;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.taltenbach.mediacaster.db.FavoriteDao;
import com.taltenbach.mediacaster.db.FavoriteDatabase;
import com.taltenbach.mediacaster.db.FavoriteEntity;
import com.taltenbach.mediacaster.util.ListItem;
import com.taltenbach.mediacaster.util.ResultCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class Favorites implements ObservableMediaList {
    private static Favorites sInstance;
    private static LoadFavoritesTask sFavoriteLoadingTask;

    private final ExecutorService mDatabaseExecutor;
    private final FavoriteDatabase mFavoriteDatabase;
    private final FavoriteDao mFavoriteDao;
    private final ObservableList<PlatformMedia> mFavoriteList;

    private ListItem<PlatformMedia> mRemovedItem;
    private RemovalCancelledCallback mRemovalCancelledCallback;

    private final ListObservers<PlatformMedia> mFavoriteListObservers;

    private Favorites(FavoriteDatabase favoriteDatabase, ExecutorService databaseExecutor, List<FavoriteEntity> favoriteList) {
        mDatabaseExecutor = databaseExecutor;
        mFavoriteDatabase = favoriteDatabase;
        mFavoriteDao = mFavoriteDatabase.favoriteDao();
        mFavoriteList = new ObservableArrayList<>();
        mFavoriteListObservers = new ListObservers<>();

        // TODO: Check if this loop is too heavy for main thread
        //       If it is, do it in the async task and make
        //       MutablePlatformMedia.getInstance thread safe
        for (FavoriteEntity favorite : favoriteList)
            mFavoriteList.add(PlatformMedia.getInstance(favorite));

        mFavoriteList.addOnListChangedCallback(mFavoriteListObservers);
    }

    @Override
    public Media getMedia(int pos) {
        return mFavoriteList.get(pos);
    }

    @Override
    public int getMediaCount() {
        return mFavoriteList.size();
    }

    @Override
    public void addListChangeObserver(@NonNull ListChangeObserver observer) {
        mFavoriteListObservers.addListChangeObserver(observer);
    }

    @Override
    public void removeListChangeObserver(ListChangeObserver observer) {
        mFavoriteListObservers.removeListChangeObserver(observer);
    }

    public PlatformMedia getFavorite(int pos) {
        return mFavoriteList.get(pos);
    }

    public void add(@NonNull PlatformMedia media) {
        ensureOpen();

        if (media.isFavorite())
            return;

        if (mRemovedItem != null && mRemovedItem.getValue().equals(media)) {
            mRemovedItem = null;

            if (mRemovalCancelledCallback != null) {
                mRemovalCancelledCallback.onRemoveCancelled();
                mRemovalCancelledCallback = null;
            }
        }

        final FavoriteEntity newFavorite = new FavoriteEntity(media);

        media.setFavorite(true);
        mFavoriteList.add(media);

        // TODO: update db info when changed ?

        mDatabaseExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mFavoriteDao.insert(newFavorite);
            }
        });
    }

    /**
     * Remove a media from the favorites
     * @param pos Position of the media to remove in the favorite list
     * @param removalCancelledCallback Callback called when the removed media is added again to the favorite list
     *                                before the removal being committed. Note that this callback is not called
     *                                if the removed favorite is restored using the {@link #restoreRemoved} method.
     * @return The removed media
     *
     * The removal is not persisted until another favorite is removed, the favorites are unloaded,
     * or {@link #commitRemove()} is manually called.
     * The removal can be undone using {@link #restoreRemoved()} until it is persisted.
     */
    public PlatformMedia remove(int pos, @Nullable RemovalCancelledCallback removalCancelledCallback) {
        ensureOpen();

        commitRemove();

        PlatformMedia media = mFavoriteList.remove(pos);
        media.setFavorite(false);

        mRemovedItem = new ListItem<>(pos, media);
        mRemovalCancelledCallback = removalCancelledCallback;

        return media;
    }

    public PlatformMedia removeNow(int pos) {
        ensureOpen();

        PlatformMedia media = mFavoriteList.remove(pos);
        media.setFavorite(false);

        commitRemoveToDatabase(media.getPlatform(), media.getPlatformKey());

        return media;
    }

    public boolean removeNow(@NonNull PlatformMedia media) {
        ensureOpen();

        if (!mFavoriteList.remove(media))
            return false;

        media.setFavorite(false);
        commitRemoveToDatabase(media.getPlatform(), media.getPlatformKey());

        return true;
    }

    public boolean restoreRemoved() {
        if (mRemovedItem == null)
            return false;

        PlatformMedia media = mRemovedItem.getValue();

        media.setFavorite(true);
        mFavoriteList.add(mRemovedItem.getPos(), media);

        mRemovedItem = null;
        mRemovalCancelledCallback = null;

        return true;
    }

    public boolean commitRemove() {
        ensureOpen();

        if (mRemovedItem == null)
            return false;

        PlatformMedia removedMedia = mRemovedItem.getValue();
        commitRemoveToDatabase(removedMedia.getPlatform(), removedMedia.getPlatformKey());

        mRemovedItem = null;
        mRemovalCancelledCallback = null;

        return true;
    }

    private void commitRemoveToDatabase(final PlatformMedia.Platform platform, final String platformKey) {
        mDatabaseExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mFavoriteDao.deleteById(platform, platformKey);
            }
        });
    }

    /*public void contains(PlatformMedia media, ResultCallback<Boolean> result) {
        ensureOpen();

        new CheckContainMediaTask(mFavoriteDao, media.getPlatform(), media.getPlatformKey(), result)
                .executeOnExecutor(mDatabaseExecutor);
    }*/

    private void closeDatabase() {
        commitRemove();

        mDatabaseExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mFavoriteDatabase.close();
            }
        });

        mDatabaseExecutor.shutdown();
    }

    private void ensureOpen() {
        if (mDatabaseExecutor.isShutdown())
            throw new IllegalStateException("Favorite database has been closed.");
    }

    /**
     * Load asynchronously the user's favorites.
     * This function is *not* thread safe and *must* be called in the main thread.
     * @param context The application context
     * @param resultCallback The callback which will be called when the favorites will be loaded
     */
    public static void load(Context context, ResultCallback<Favorites> resultCallback) {
        if (sInstance != null) {
            resultCallback.onResult(sInstance);
            return;
        }

        if (sFavoriteLoadingTask != null) {
            sFavoriteLoadingTask.addResultCallback(resultCallback);
            return;
        }

        FavoriteDatabase favoriteDatabase = FavoriteDatabase.getInstance(context);

        ExecutorService databaseExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(@NonNull Runnable r) {
                return new Thread(r, "FavoriteDatabaseThread");
            }
        });

        sFavoriteLoadingTask = new LoadFavoritesTask(databaseExecutor, favoriteDatabase, resultCallback);

        sFavoriteLoadingTask.executeOnExecutor(databaseExecutor);
    }

    public static void close() {
        if (sInstance != null)
            sInstance.closeDatabase();

        sInstance = null;
    }

    public interface RemovalCancelledCallback {
        void onRemoveCancelled();
    }

    private static class LoadFavoritesTask extends AsyncTask<Void, Void, List<FavoriteEntity>> {
        private final ExecutorService mDatabaseExecutor;
        private final FavoriteDatabase mFavoriteDatabase;
        private final List<ResultCallback<Favorites>> mResultCallbacks;

        LoadFavoritesTask(ExecutorService databaseExecutor, FavoriteDatabase favoriteDatabase, ResultCallback<Favorites> callback) {
            mDatabaseExecutor = databaseExecutor;
            mFavoriteDatabase = favoriteDatabase;
            mResultCallbacks = new ArrayList<>();

            mResultCallbacks.add(callback);
        }

        void addResultCallback(ResultCallback<Favorites> resultCallback) {
            mResultCallbacks.add(resultCallback);
        }

        @Override
        protected List<FavoriteEntity> doInBackground(Void... voids) {
            return mFavoriteDatabase.favoriteDao().getAll();
        }

        @Override
        protected void onPostExecute(List<FavoriteEntity> favoriteEntities) {
            sInstance = new Favorites(mFavoriteDatabase, mDatabaseExecutor, favoriteEntities);

            sFavoriteLoadingTask = null;

            for (ResultCallback<Favorites> callback : mResultCallbacks)
                callback.onResult(sInstance);
        }
    }

    /*private static final class CheckContainMediaTask extends AsyncTask<FavoriteDao, Void, Boolean> {
        private final FavoriteDao mFavoriteDao;
        private final PlatformMedia.Platform mMediaPlatform;
        private final String mMediaPlatformKey;
        private final ResultCallback<Boolean> mResultCallback;

        CheckContainMediaTask(FavoriteDao favoriteDao, PlatformMedia.Platform mediaPlatform,
                              String mediaPlatformKey, ResultCallback<Boolean> resultCallback)
        {
            mFavoriteDao = favoriteDao;
            mMediaPlatform = mediaPlatform;
            mMediaPlatformKey = mediaPlatformKey;
            mResultCallback = resultCallback;
        }

        @Override
        protected Boolean doInBackground(FavoriteDao... favoriteDaos) {
            return mFavoriteDao.getById(mMediaPlatform, mMediaPlatformKey) != null;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mResultCallback.onResult(result);
        }
    }*/
}
