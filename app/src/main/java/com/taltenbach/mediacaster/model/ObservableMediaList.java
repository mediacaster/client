package com.taltenbach.mediacaster.model;

import android.support.annotation.NonNull;

public interface ObservableMediaList {
    Media getMedia(int pos);

    int getMediaCount();

    void addListChangeObserver(@NonNull ListChangeObserver observer);

    void removeListChangeObserver(ListChangeObserver observer);

    interface ListChangeObserver {
        void onItemRangeInserted(int positionStart, int itemCount);

        void onItemRangeMoved(int fromPosition, int toPosition, int itemCount);

        void onItemRangeRemoved(int positionStart, int itemCount);
    }
}
