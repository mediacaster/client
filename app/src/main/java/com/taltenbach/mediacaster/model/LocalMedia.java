package com.taltenbach.mediacaster.model;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import com.taltenbach.mediacaster.client.ClientInputStream;
import com.taltenbach.mediacaster.util.loading.BitmapImageLoader;
import com.taltenbach.mediacaster.util.loading.ImageLoader;

import java.io.IOException;

public final class LocalMedia extends BaseMedia {
    private final Bitmap mThumbnail;

    private LocalMedia(MediaType mediaType, String title, @Nullable Bitmap thumbnail) {
        super(mediaType, title);

        mThumbnail = thumbnail;
    }

    @Override
    public ImageLoader loadThumbnail() {
        return new BitmapImageLoader(mThumbnail, getMediaType().getDefaultThumbnailResId());
    }

    @Override
    public void acceptVisitor(MediaVisitor visitor) {
        visitor.visit(this);
    }

    public Bitmap getThumbnail() {
        return mThumbnail;
    }

    static final class Builder extends BaseMedia.Builder<LocalMedia> {
        private final Bitmap mThumbnail;

        Builder(ClientInputStream in) throws IOException {
            super(in);

            mThumbnail = in.readBitmap();
        }

        @Override
        LocalMedia build() {
            return new LocalMedia(getMediaType(), getTitle(), mThumbnail);
        }
    }
}
