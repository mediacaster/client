package com.taltenbach.mediacaster.model;

import android.util.SparseArray;

import com.taltenbach.mediacaster.client.ClientInputStream;

import java.io.IOException;

public final class PlaylistEntry {
    private final Media mMedia;
    private final long mEntryId;
    private final String mAdderName;

    private PlaylistEntry(Media media, long entryId, String adderName) {
        mMedia = media;
        mEntryId = entryId;
        mAdderName = adderName;
    }

    public Media getMedia() {
        return mMedia;
    }

    public long getEntryId() {
        return mEntryId;
    }

    public String getAdderName() {
        return mAdderName;
    }
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PlaylistEntry))
			return false;
		
		if (obj == this)
			return true;
		
		PlaylistEntry other = (PlaylistEntry) obj;
		return mEntryId == other.mEntryId;
	}
	
	@Override
	public int hashCode() {
		return (int) (mEntryId ^ (mEntryId >>> 32));
	}

    public static final class Builder {
        private final MediaLocation mMediaLocation;
        private final BaseMedia.Builder mMediaBuilder;
        private final long mEntryId;
        private final String mAdderName;

        private enum MediaLocation {
            LOCAL (0),
            PLATFORM (1);

            private static final SparseArray<MediaLocation> VALUES = new SparseArray<>();

            static {
                for (MediaLocation l : MediaLocation.values())
                    VALUES.append(l.mId, l);
            }

            private final short mId;

            MediaLocation(int id) {
                mId = (short) id;
            }

            public short getId() {
                return mId;
            }

            public static MediaLocation fromId(short id) {
                return VALUES.get(id);
            }
        }

        public Builder(ClientInputStream in) throws IOException {
            mMediaLocation = MediaLocation.fromId(in.readShort());

            if (mMediaLocation == MediaLocation.LOCAL)
                mMediaBuilder = new LocalMedia.Builder(in);
            else
                mMediaBuilder = new PlatformMedia.Builder(in);

            mEntryId = in.readLong();
            mAdderName = in.readString();
        }

        /**
         * Returns the PlaylistEntry corresponding to the information provided to this builder.
         * Note that this method *must* be called in the main thread.
         *
         * @return The PlaylistEntry corresponding to the information provided to this builder.
         */
        public PlaylistEntry build() {
            return new PlaylistEntry(mMediaBuilder.build(), mEntryId, mAdderName);
        }
    }
}
