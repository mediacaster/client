package com.taltenbach.mediacaster.model;

import android.databinding.ObservableList;
import android.support.annotation.NonNull;

import java.util.HashSet;
import java.util.Set;

class ListObservers<T> extends ObservableList.OnListChangedCallback<ObservableList<T>> {
    private final Set<ObservableMediaList.ListChangeObserver> mListChangeObservers;

    ListObservers() {
        mListChangeObservers = new HashSet<>();
    }

    void addListChangeObserver(@NonNull ObservableMediaList.ListChangeObserver observer) {
        mListChangeObservers.add(observer);
    }

    void removeListChangeObserver(ObservableMediaList.ListChangeObserver observer) {
        mListChangeObservers.remove(observer);
    }

    @Override
    public void onChanged(ObservableList<T> sender) {
        // Empty
    }

    @Override
    public void onItemRangeChanged(ObservableList<T> sender, int positionStart, int itemCount) {
        // Empty
    }

    @Override
    public void onItemRangeInserted(ObservableList<T> sender, int positionStart, int itemCount) {
        for (ObservableMediaList.ListChangeObserver o : mListChangeObservers)
            o.onItemRangeInserted(positionStart, itemCount);
    }

    @Override
    public void onItemRangeMoved(ObservableList<T> sender, int fromPosition, int toPosition, int itemCount) {
        for (ObservableMediaList.ListChangeObserver o : mListChangeObservers)
            o.onItemRangeMoved(fromPosition, toPosition, itemCount);
    }

    @Override
    public void onItemRangeRemoved(ObservableList<T> sender, int positionStart, int itemCount) {
        for (ObservableMediaList.ListChangeObserver o : mListChangeObservers)
            o.onItemRangeRemoved(positionStart, itemCount);
    }
}
