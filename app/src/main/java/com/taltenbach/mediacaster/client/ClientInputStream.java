package com.taltenbach.mediacaster.client;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public final class ClientInputStream {
    private final DataInputStream mIn;

    ClientInputStream(InputStream in) {
        mIn = new DataInputStream(in);
    }

    public boolean readBoolean() throws IOException {
        return mIn.readBoolean();
    }

    public byte readByte() throws IOException {
        return mIn.readByte();
    }

    public short readShort() throws IOException {
        return mIn.readShort();
    }

    public int readInt() throws IOException {
        return mIn.readInt();
    }

    public long readLong() throws IOException {
        return mIn.readLong();
    }

    public float readFloat() throws IOException {
        return mIn.readFloat();
    }

    public double readDouble() throws IOException {
        return mIn.readDouble();
    }

    public char readChar() throws IOException {
        return mIn.readChar();
    }

    public Bitmap readBitmap() throws IOException {
        // TODO : Check if BitmapFactory decode PNG format
        return BitmapFactory.decodeStream(mIn);
    }

    public String readString() throws IOException {
        byte[] bytes = new byte[mIn.readInt()];
        mIn.readFully(bytes);
        return new String(bytes, Charset.forName("UTF-16BE"));
    }
}
