package com.taltenbach.mediacaster.client;

import android.util.SparseArray;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.client
 * Creator : Thomas ALTENBACH
 * Date : 09/07/2018
 */

enum MessageHeader {
    ERROR_ENQUEUE_FAILED(-2),
    ERROR_CONNECTION_FAILED(-1),
    CLIENT_INFO(1),
    CONNECTION_ACCEPTED(2),
    CLIENT_KICKED(3),
    CLIENT_BANNED(4),
    SERVER_STOPPED(5),
    ENTRY_ENQUEUED(6),
    ENTRY_REMOVED(7),
    ENTRY_MOVED_UP(8),
    ENTRY_MOVED_DOWN(9),
    CURRENT_ENTRY_CHANGED(10),
    OUT_OF_MEDIA(11),
    ENQUEUE_MEDIA_URL(12),
    ENQUEUE_MEDIA_ID(13),
    REMOVE_ENTRY(14),
    KICK_USER(15),
    BAN_USER(16),
    PLAY_ENTRY(17),
    VOTE_TO_SKIP_CURRENT(18),
    SKIP_VOTE_UPDATED(19),
    PING(20),
    PONG(21),
    PLAYER_STATE_CHANGED(22),
    RESUME_PLAYBACK(23),
    PAUSE_PLAYBACK(24),
    SEEK_TO(25),
    SKIP_NEXT(26),
    SKIP_PREVIOUS(27),
    VOLUME_CHANGED(28),
    CHANGE_VOLUME(29)/*,
    MOVE_VIDEO_UP(14),
    MOVE_VIDEO_DOWN(15)*/;

    private static final SparseArray<MessageHeader> VALUES = new SparseArray<>();

    static {
        for (MessageHeader h : MessageHeader.values())
            VALUES.append(h.mId, h);
    }

    private final short mId;

    MessageHeader(int id) {
        mId = (short) id;
    }

    public short getId() {
        return mId;
    }

    public static MessageHeader fromId(short id) {
        return VALUES.get(id);
    }
}
