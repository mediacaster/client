package com.taltenbach.mediacaster.client;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.ui.MainActivity;
import com.taltenbach.mediacaster.util.NotificationUtil;

import java.util.HashSet;
import java.util.Set;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.client
 * Creator : Thomas ALTENBACH
 * Date : 16/08/2018
 */
final class SkipVote {
    private static final String ACTION_VOTE_YES = "com.taltenbach.mediacaster.client.SkipVote.ACTION_VOTE_YES";
    private static final String ACTION_VOTE_NO = "com.taltenbach.mediacaster.client.SkipVote.ACTION_VOTE_NO";
    private static final String VOTE_NOTIFICATION_CHANNEL_ID = "com.taltenbach.mediacaster.client.SkipVote.VOTE_NOTIFICATION_CHANNEL";
    private static final int SKIP_VOTE_NOTIFICATION_ID = 44;

    private final NotificationCompat.Builder mNotificationBuilder;
    private final UserVoteCallback mUserVoteCallback;

    private SkipVoteReceiver mVoteReceiver;

    SkipVote(@NonNull Context context, int favorableVotes, int favorableVotesNeeded, @NonNull UserVoteCallback userVoteCallback) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationUtil.createChannel(
                    context,
                    VOTE_NOTIFICATION_CHANNEL_ID,
                    R.string.playlist_service_vote_notification_channel_name,
                    R.string.playlist_service_vote_notification_channel_desc,
                    NotificationManager.IMPORTANCE_HIGH);
        }

        mNotificationBuilder = createSkipVoteNotificationBuilder(context);
        mVoteReceiver = new SkipVoteReceiver();
        mUserVoteCallback = userVoteCallback;

        IntentFilter voteFilter = new IntentFilter();
        voteFilter.addAction(ACTION_VOTE_YES);
        voteFilter.addAction(ACTION_VOTE_NO);

        context.registerReceiver(mVoteReceiver, voteFilter);

        update(context, favorableVotes, favorableVotesNeeded);
    }

    private NotificationCompat.Builder createSkipVoteNotificationBuilder(@NonNull Context c) {
        Intent notifIntent = new Intent(c, MainActivity.class);
        PendingIntent notifPendingIntent = PendingIntent.getActivity(c, 0, notifIntent, 0);

        PendingIntent voteYesPendingIntent = PendingIntent.getBroadcast(c, 0, new Intent(ACTION_VOTE_YES), PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent voteNoPendingIntent = PendingIntent.getBroadcast(c, 0, new Intent(ACTION_VOTE_NO), PendingIntent.FLAG_CANCEL_CURRENT);

        return new NotificationCompat.Builder(c, VOTE_NOTIFICATION_CHANNEL_ID)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_vote_white_24)
                .setContentTitle(c.getText(R.string.skip_current_media))
                .setContentIntent(notifPendingIntent)
                .setOnlyAlertOnce(true)
                .addAction(R.drawable.ic_thumb_up_white_24, c.getText(R.string.vote_yes), voteYesPendingIntent)
                .addAction(R.drawable.ic_thumb_down_white_24, c.getText(R.string.vote_no), voteNoPendingIntent);
    }

    private void unregisterVoteReceiver(@NonNull Context c) {
        if (mVoteReceiver == null)
            return;

        c.unregisterReceiver(mVoteReceiver);
        mVoteReceiver = null;
    }

    void update(@NonNull Context c, int favorableVotes, int favorableVotesNeeded) {
        mNotificationBuilder
                .setProgress(favorableVotesNeeded, favorableVotes, false)
                .setContentText(favorableVotes + "/" + favorableVotesNeeded)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(c.getString(R.string.favorable_votes_left, favorableVotesNeeded - 1)));

        NotificationUtil.notify(c, SKIP_VOTE_NOTIFICATION_ID, mNotificationBuilder.build());
    }

    void disableUserVote(@NonNull Context c) {
        unregisterVoteReceiver(c);

        mNotificationBuilder.mActions.clear();
        NotificationUtil.notify(c, SKIP_VOTE_NOTIFICATION_ID, mNotificationBuilder.build());
    }

    void conclude(@NonNull Context c) {
        NotificationUtil.cancel(c, SKIP_VOTE_NOTIFICATION_ID);

        unregisterVoteReceiver(c);
    }

    interface UserVoteCallback {
        void onUserVotedYes();
    }

    public final class SkipVoteReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            disableUserVote(context);

            if (ACTION_VOTE_YES.equals(intent.getAction()))
                mUserVoteCallback.onUserVotedYes();
        }
    }
}
