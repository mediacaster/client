package com.taltenbach.mediacaster.client;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;

import com.taltenbach.mediacaster.model.PlaylistEntry;

import java.io.IOException;
import java.io.InputStream;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.client
 * Creator : Thomas ALTENBACH
 * Date : 08/07/2018
 */

class ServerListenerRunnable implements Runnable {
    private final PlaylistClient mClient;
    private final ClientInputStream mIn;
    private final Handler mMainHandler;
    private final ClientCallback mCallback;

    ServerListenerRunnable(@NonNull PlaylistClient client, @NonNull InputStream in, @NonNull ClientCallback callback) {
        mClient = client;
        mIn = new ClientInputStream(in);
        mMainHandler = new Handler(Looper.getMainLooper());
        mCallback = callback;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                MessageHeader msgHeader = MessageHeader.fromId(mIn.readShort());

                if (msgHeader == null) {
                    Log.e(getClass().getSimpleName(), "Invalid message header id.");
                    continue;
                }

                switch (msgHeader) {
                    case ERROR_CONNECTION_FAILED:
                        connectionFailed();
                        return;
                    case CLIENT_KICKED:
                        kicked();
                        return;
                    case CLIENT_BANNED:
                        banned();
                        return;
                    case SERVER_STOPPED:
                        serverStopped();
                        return;
                    case CONNECTION_ACCEPTED:
                        connectionAccepted();
                        break;
                    case ENTRY_ENQUEUED:
                        entryEnqueued();
                        break;
                    case ENTRY_REMOVED:
                        entryRemoved();
                        break;
                    case ENTRY_MOVED_UP:
                        entryMovedUp();
                        break;
                    case ENTRY_MOVED_DOWN:
                        entryMovedDown();
                        break;
                    case CURRENT_ENTRY_CHANGED:
                        currentEntryChanged();
                        break;
                    case OUT_OF_MEDIA:
                        outOfMedia();
                        break;
                    case ERROR_ENQUEUE_FAILED:
                        enqueueFailed();
                        break;
                    case SKIP_VOTE_UPDATED:
                        skipVoteUpdated();
                        break;
                    case PING:
                        mClient.sendMessage(MessageHeader.PONG);
                        break;
                    case PONG:
                        pong();
                        break;
                    case PLAYER_STATE_CHANGED:
                        playerStateChanged();
                        break;
                    case VOLUME_CHANGED:
                        volumeChanged();
                        break;
                    default:
                        Log.e(getClass().getSimpleName(), "Unable to handle message " + msgHeader.name());
                        break;
                }
            }
        } catch (IOException e) {
            Log.i(getClass().getSimpleName(), "Connection lost.", e);

            if (Thread.interrupted())
                return; // Socket was manually closed by the user

            final Thread listenerThread = Thread.currentThread();

            mMainHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.d("CloseDebug", "Connection lost, interrupted is " + listenerThread.isInterrupted());

                    if (listenerThread.isInterrupted())
                        return; // Socket was manually closed by the user

                    // Socket was closed because the server process has been killed or the internet connection has been lost
                    mClient.close();
                    mCallback.onConnectionLost();
                }
            });
        } finally {
            // Ensure the socket is closed
            mMainHandler.post(new Runnable() {
                @Override
                public void run() {
                    mClient.close();
                }
            });
        }
    }

    private void connectionAccepted() throws IOException {
        final boolean isAdmin = mIn.readBoolean();
        final boolean isSkipVoteEnabled = mIn.readBoolean();
        final int currentEntryIndex = mIn.readInt();
        final MediaPlayerController.PlayerState playerState = MediaPlayerController.PlayerState.fromId(mIn.readShort());
        final long currentMediaTime = mIn.readLong();
        final long mediaDuration = mIn.readLong();
        final int remoteVolume = mIn.readInt();
        final int entryCount = mIn.readInt();
        final PlaylistEntry.Builder[] entryBuilders = new PlaylistEntry.Builder[entryCount];

        for (int i = 0; i < entryCount; ++i)
            entryBuilders[i] = new PlaylistEntry.Builder(mIn);

        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                PlaylistEntry[] entries = new PlaylistEntry[entryCount];

                for (int i = 0; i < entryCount; ++i)
                    entries[i] = entryBuilders[i].build();

                mCallback.onConnected(mClient.getConnectionInfo().getUserName(), isAdmin, isSkipVoteEnabled,
                        entries, currentEntryIndex, playerState, currentMediaTime, mediaDuration, remoteVolume);
            }
        });
    }

    private void connectionFailed() throws IOException {
        final ConnectionFailedReason reason = ConnectionFailedReason.fromId(mIn.readShort());

        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onConnectionFailed(reason);
            }
        });
    }

    private void kicked() {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onKicked();
            }
        });
    }

    private void banned() {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBanned();
            }
        });
    }

    private void serverStopped() {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onServerStopped();
            }
        });
    }

    private void entryEnqueued() throws IOException {
        final PlaylistEntry.Builder newEntry = new PlaylistEntry.Builder(mIn);

        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEntryEnqueued(newEntry.build());
            }
        });
    }

    private void entryRemoved() throws IOException {
        final PlaylistEntryReference entryInfo = new PlaylistEntryReference(mIn);

        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEntryRemoved(entryInfo);
            }
        });
    }

    private void entryMovedUp() throws IOException {
        final PlaylistEntryReference entryInfo = new PlaylistEntryReference(mIn);

        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEntryMovedUp(entryInfo);
            }
        });
    }

    private void entryMovedDown() throws IOException {
        final PlaylistEntryReference entryInfo = new PlaylistEntryReference(mIn);

        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEntryMovedDown(entryInfo);
            }
        });
    }

    private void currentEntryChanged() throws IOException {
        final PlaylistEntryReference entryInfo = new PlaylistEntryReference(mIn);

        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCurrentEntryChanged(entryInfo);
            }
        });
    }

    private void outOfMedia() {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOutOfMedia();
            }
        });
    }

    private void enqueueFailed() throws IOException {
        final String errorMsg = mIn.readString();

        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEnqueueFailed(errorMsg);
            }
        });
    }

    private void skipVoteUpdated() throws IOException {
        final int favorableVotes = mIn.readInt();
        final int favorableVotesNeeded = mIn.readInt();

        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSkipVoteUpdated(favorableVotes, favorableVotesNeeded);
            }
        });
    }

    private void pong() {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPongReceived();
            }
        });
    }

    private void playerStateChanged() throws IOException {
        final MediaPlayerController.PlayerState playerState = MediaPlayerController.PlayerState.fromId(mIn.readShort());
        final long currentMediaTime = mIn.readLong();
        final long mediaDuration = mIn.readLong();

        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlayerStateChanged(playerState, currentMediaTime, mediaDuration);
            }
        });
    }

    private void volumeChanged() throws IOException {
        final int volume = mIn.readInt();

        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRemoteVolumeChanged(volume);
            }
        });
    }
}
