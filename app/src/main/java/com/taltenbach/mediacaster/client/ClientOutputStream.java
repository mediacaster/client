package com.taltenbach.mediacaster.client;

import android.support.annotation.NonNull;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

final class ClientOutputStream {
    private final DataOutputStream mOut;

    ClientOutputStream(OutputStream out) {
        mOut = new DataOutputStream(out);
    }

    void writeBoolean(boolean value) throws IOException {
        mOut.writeBoolean(value);
    }

    void writeByte(byte value) throws IOException {
        mOut.writeByte(value);
    }

    void writeShort(short value) throws IOException {
        mOut.writeShort(value);
    }

    void writeInt(int value) throws IOException {
        mOut.writeInt(value);
    }

    void writeLong(long value) throws IOException {
        mOut.writeLong(value);
    }

    void writeFloat(float value) throws IOException {
        mOut.writeFloat(value);
    }

    void writeDouble(double value) throws IOException {
        mOut.writeDouble(value);
    }

    void writeChar(char value) throws IOException {
        mOut.writeChar(value);
    }

    void writeString(@NonNull String value) throws IOException {
        byte[] bytes = value.getBytes(Charset.forName("UTF-16BE"));

        mOut.writeInt(bytes.length);
        mOut.write(bytes);
    }

    void writeParcel(ClientParcel parcel) throws IOException {
        parcel.writeParcelTo(this);
    }
}
