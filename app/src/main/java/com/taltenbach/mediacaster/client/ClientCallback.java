package com.taltenbach.mediacaster.client;

import com.taltenbach.mediacaster.model.PlaylistEntry;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.client
 * Creator : Thomas ALTENBACH
 * Date : 08/07/2018
 */

interface ClientCallback {
    void onConnected(String userName, boolean isUserAdmin, boolean isSkipVoteEnabled, PlaylistEntry[] initialEntries,
                     int currentEntryIndex, MediaPlayerController.PlayerState playerState, long currentMediaTime,
                     long mediaDuration, int remoteVolume);

    void onConnectionFailed(ConnectionFailedReason reason);

    void onKicked();

    void onBanned();

    void onServerStopped();

    void onConnectionLost();

    void onEntryEnqueued(PlaylistEntry newEntry);

    void onEntryRemoved(PlaylistEntryReference entryRef);

    void onEntryMovedUp(PlaylistEntryReference entryRef);

    void onEntryMovedDown(PlaylistEntryReference entryRef);

    void onCurrentEntryChanged(PlaylistEntryReference newCurrentEntryRef);

    void onOutOfMedia();

    void onEnqueueFailed(String errorMsg);

    void onSkipVoteUpdated(int favorableVotes, int favorableVotesNeeded);

    void onPongReceived();

    void onPlayerStateChanged(MediaPlayerController.PlayerState playerState, long currentMediaTime, long mediaDuration);

    void onRemoteVolumeChanged(int volume);
}
