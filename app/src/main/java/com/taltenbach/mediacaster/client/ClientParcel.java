package com.taltenbach.mediacaster.client;

import java.io.IOException;

interface ClientParcel {
    /**
     * Write the parcel to the specified output stream.
     * Note that this function will *not* be called in the main thread and
     * must therefore be synchronized with the methods that modify the object
     * state.
     *
     * @param out The output stream
     * @throws IOException if an I/O error occurs
     */
    void writeParcelTo(ClientOutputStream out) throws IOException;
}
