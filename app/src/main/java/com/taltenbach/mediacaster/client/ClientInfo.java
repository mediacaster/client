package com.taltenbach.mediacaster.client;

import java.io.IOException;

final class ClientInfo implements ClientParcel {
    private final short mProtocolVersion;
    private final String mName;
    private final String mId;
    private final String mAdminPassword;

    ClientInfo(short protocolVersion, String name, String id, String adminPassword) {
        mProtocolVersion = protocolVersion;
        mName = name;
        mId = id;
        mAdminPassword = adminPassword;
    }

    short getProtocolVersion() {
        return mProtocolVersion;
    }

    String getName() {
        return mName;
    }

    String getId() {
        return mId;
    }

    String getAdminPassword() {
        return mAdminPassword;
    }

    @Override
    public void writeParcelTo(ClientOutputStream out) throws IOException {
        out.writeShort(mProtocolVersion);
        out.writeString(mName);
        out.writeString(mId);
        out.writeString(mAdminPassword);
    }
}
