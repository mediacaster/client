package com.taltenbach.mediacaster.client;

import java.io.IOException;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.client
 * Creator : Thomas ALTENBACH
 * Date : 08/07/2018
 */

final class PlaylistEntryReference implements ClientParcel {
    private final long mEntryId;
    private final int mEntryIndex;

    PlaylistEntryReference(long entryId, int entryIndex) {
        mEntryId = entryId;
        mEntryIndex = entryIndex;
    }

    PlaylistEntryReference(ClientInputStream in) throws IOException {
        mEntryId = in.readLong();
        mEntryIndex = in.readInt();
    }

    public long getEntryId() {
        return mEntryId;
    }

    public int getEntryIndex() {
        return mEntryIndex;
    }

    @Override
    public void writeParcelTo(ClientOutputStream out) throws IOException {
        out.writeLong(mEntryId);
        out.writeInt(mEntryIndex);
    }
}
