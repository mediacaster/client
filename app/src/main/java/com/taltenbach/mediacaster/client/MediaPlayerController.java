package com.taltenbach.mediacaster.client;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.SparseArray;

import com.taltenbach.mediacaster.model.ObservableMediaList;
import com.taltenbach.mediacaster.model.Playlist;
import com.taltenbach.mediacaster.model.PlaylistEntry;

import java.io.IOException;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.client
 * Creator : Thomas ALTENBACH
 * Date : 20/08/2018
 */

public final class MediaPlayerController {
    private final PlaylistClient mPlaylistClient;
    private final Playlist mPlaylist;
    private final MediaTimer mMediaTimer;

    private PlayerState mState;
    private boolean mHasNextEntry;
    private boolean mHasPreviousEntry;
    private long mMediaDuration;
    private int mRemoteVolume;
    private MediaPlayerObserver mObserver;

    public enum PlayerState {
        IDLE(0),
        LOADING(1),
        PLAYING(2),
        PAUSED(3);

        private static final SparseArray<PlayerState> VALUES = new SparseArray<>();

        static {
            for (PlayerState state : PlayerState.values())
                VALUES.append(state.mId, state);
        }

        private final short mId;

        PlayerState(int id) {
            mId = (short) id;
        }

        short getId() {
            return mId;
        }

        static PlayerState fromId(short id) {
            return VALUES.get(id);
        }
    }

    MediaPlayerController(@NonNull PlaylistClient playlistClient, @NonNull Playlist playlist, @NonNull ClientClock clientClock) {
        mPlaylistClient = playlistClient;
        mPlaylist = playlist;
        mState = PlayerState.IDLE;
        mHasPreviousEntry = playlist.hasPreviousEntry();
        mHasNextEntry = playlist.hasNextEntry();

        mMediaTimer = new MediaTimer(clientClock, new MediaTimer.UpdateCallback() {
            @Override
            public void onUpdate(long currentTime) {
                mObserver.onMediaTimeChanged(currentTime);
            }
        });

        mMediaTimer.setCallbackEnabled(false);

        playlist.addCurrentEntryChangeObserver(new Playlist.CurrentEntryChangeObserver() {
            @Override
            public void onCurrentEntryChanged(int newEntryIndex, PlaylistEntry newCurrentEntry) {
                updateNavigationDirections();
            }
        });

        playlist.addListChangeObserver(new ObservableMediaList.ListChangeObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                updateNavigationDirections();
            }

            @Override
            public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
                updateNavigationDirections();
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                updateNavigationDirections();
            }
        });
    }

    public void requestResume() {
        if (mState != PlayerState.PAUSED)
            throw new IllegalStateException("Player is not paused.");

        mPlaylistClient.sendMessage(MessageHeader.RESUME_PLAYBACK);
    }

    public void requestPause() {
        if (mState != PlayerState.PLAYING)
            throw new IllegalStateException("Player is not playing.");

        mPlaylistClient.sendMessage(MessageHeader.PAUSE_PLAYBACK);
    }

    public void requestSeekTo(final int seconds) {
        if (mState == PlayerState.IDLE)
            throw new IllegalStateException("Player has no current entry.");

       if (seconds > mMediaDuration)
           throw new IllegalArgumentException("The number of seconds cannot be greater than the media duration.");

       final long currentEntryId = mPlaylist.getCurrentEntry().getEntryId();

        mPlaylistClient.sendMessage(MessageHeader.SEEK_TO, new ClientParcel() {
            @Override
            public void writeParcelTo(ClientOutputStream out) throws IOException {
                out.writeLong(currentEntryId);
                out.writeInt(seconds);
            }
        });
    }

    public void requestSkipNext() {
        if (!mHasNextEntry)
            throw new IllegalStateException("There is no next entry.");

        final long currentEntryId = mPlaylist.getCurrentEntry().getEntryId();

        mPlaylistClient.sendMessage(MessageHeader.SKIP_NEXT, new ClientParcel() {
            @Override
            public void writeParcelTo(ClientOutputStream out) throws IOException {
                out.writeLong(currentEntryId);
            }
        });
    }

    public void requestSkipPrevious() {
        if (!mHasPreviousEntry)
            throw new IllegalStateException("There is no previous entry.");

        PlaylistEntry currentEntry = mPlaylist.getCurrentEntry();
        final long currentEntryId = currentEntry == null ? -1 : currentEntry.getEntryId();

        mPlaylistClient.sendMessage(MessageHeader.SKIP_PREVIOUS, new ClientParcel() {
            @Override
            public void writeParcelTo(ClientOutputStream out) throws IOException {
                out.writeLong(currentEntryId);
            }
        });
    }

    /**
     * Changes the remote volume of the media.
     * @param volume The volume in percentage of the maximum volume
     */
    public void requestVolumeChange(final int volume) {
        if (volume < 0 || volume > 100)
            throw new IllegalArgumentException("Volume must be between 0 and 100.");

        mPlaylistClient.sendMessage(MessageHeader.CHANGE_VOLUME, new ClientParcel() {
            @Override
            public void writeParcelTo(ClientOutputStream out) throws IOException {
                out.writeInt(volume);
            }
        });
    }

    public void setMediaPlayerObserver(@Nullable MediaPlayerObserver observer) {
        mMediaTimer.setCallbackEnabled(observer != null);
        mObserver = observer;
    }

    public PlayerState getPlayerState() {
        return mState;
    }

    public long getCurrentMediaTime() {
        if (mState == PlayerState.IDLE)
            throw new IllegalStateException("The player has no current media.");

        return mMediaTimer.isStarted() ? mMediaTimer.getCurrentMediaTime() : 0;
    }

    /**
     * Gets the current media duration.
     * This method can only be called if the player has a current media (ie. is not IDLE).
     * @return The current media duration (in ms)
     */
    public long getMediaDuration() {
        if (mState == PlayerState.IDLE)
            throw new IllegalStateException("The player has no current media.");

        return mMediaDuration;
    }

    public int getRemoteVolume() {
        return mRemoteVolume;
    }

    public boolean hasPreviousEntry() {
        return mHasPreviousEntry;
    }

    public boolean hasNextEntry() {
        return mHasNextEntry;
    }

    /**
     * Notifies the player controller that the server player state has changed.
     * @param state The new player state
     * @param currentMediaTime The current media time (in ms)
     * @param mediaDuration The media duration (in ms)
     */
    void notifyPlayerStateChanged(@NonNull PlayerState state, long currentMediaTime, long mediaDuration) {
        mState = state;

        updateDuration(mediaDuration);

        switch (state) {
            case IDLE:
                mMediaTimer.stop();

                if (mObserver != null)
                    mObserver.onIdle();

                break;
            case LOADING:
                mMediaTimer.pauseAt(currentMediaTime);

                if (mObserver != null)
                    mObserver.onLoading();

                break;
            case PLAYING:
                if (mMediaTimer.isRunning())
                    mMediaTimer.stop();

                mMediaTimer.startAt(currentMediaTime, mediaDuration);

                if (mObserver != null)
                    mObserver.onPlaying();

                break;
            case PAUSED:
                mMediaTimer.pauseAt(currentMediaTime);

                if (mObserver != null)
                    mObserver.onPaused();

                break;
        }
    }

    void notifyRemoteVolumeChanged(int volume) {
        mRemoteVolume = volume;

        if (mObserver != null)
            mObserver.onRemoteVolumeChanged(volume);
    }

    private void updateNavigationDirections() {
        if (mHasNextEntry != mPlaylist.hasNextEntry()) {
            mHasNextEntry = !mHasNextEntry;

            if (mObserver != null)
                mObserver.onHasNextEntryChanged(mHasNextEntry);
        }

        if (mHasPreviousEntry != mPlaylist.hasPreviousEntry()) {
            mHasPreviousEntry = !mHasPreviousEntry;

            if (mObserver != null)
                mObserver.onHasPreviousEntryChanged(mHasPreviousEntry);
        }
    }

    private void updateDuration(long mediaDuration) {
        if (mediaDuration == mMediaDuration)
            return;

        mMediaDuration = mediaDuration;

        if (mObserver != null)
            mObserver.onMediaDurationChanged(mediaDuration);
    }

    public interface MediaPlayerObserver {
        void onIdle();

        void onLoading();

        void onPlaying();

        void onPaused();

        void onMediaTimeChanged(long currentMediaTime);

        void onMediaDurationChanged(long duration);

        void onHasNextEntryChanged(boolean hasNextEntry);

        void onHasPreviousEntryChanged(boolean hasPreviousEntry);

        void onRemoteVolumeChanged(int volume);
    }
}
