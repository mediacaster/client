package com.taltenbach.mediacaster.client;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.taltenbach.mediacaster.model.MutablePlaylist;
import com.taltenbach.mediacaster.model.PlatformMedia;
import com.taltenbach.mediacaster.model.Playlist;
import com.taltenbach.mediacaster.model.PlaylistEntry;
import com.taltenbach.mediacaster.util.ListItem;

public class ClientPlaylistController {
    private final String mUserName;
    private final boolean mIsUserAdmin;
    private final boolean mIsSkipVoteEnabled;
    private final MutablePlaylist mPlaylist;

    private RemovedEntry mRemovedEntry;
    private RemovedByServerCallback mRemovedByServerCallback;

    private PlaylistChangedByUserObserver mPlaylistChangedByUserObserver;

    private boolean mHasUserVoted;
    private UserSkipVoteObserver mUserSkipVoteObserver;

    /**
     * Create a new playlist controller.
     * @param userName Name of the user
     * @param isUserAdmin true if the user has admin rights, false otherwise
     * @param isSkipVoteEnabled true if the user can vote to skip the currently playing entry
     * @param initialEntries Initial playlist entries
     * @param currentEntryIndex Index of the initial current entry (Playlist.NO_INDEX if none)
     */
    ClientPlaylistController(String userName, boolean isUserAdmin, boolean isSkipVoteEnabled, @Nullable PlaylistEntry[] initialEntries, int currentEntryIndex) {
        mUserName = userName;
        mIsUserAdmin = isUserAdmin;
        mIsSkipVoteEnabled = isSkipVoteEnabled;
        mPlaylist = new MutablePlaylist(initialEntries, currentEntryIndex);
    }

    public String getUserName() {
        return mUserName;
    }

    public Playlist getPlaylist() {
        return mPlaylist;
    }

    public boolean isUserAdmin() {
        return mIsUserAdmin;
    }

    public void requestPlatformMediaEnqueue(@NonNull String mediaUrl) {
        if (mPlaylistChangedByUserObserver == null)
            throw new IllegalStateException("Not connected to server.");

        mPlaylistChangedByUserObserver.onUserRequestsPlatformMediaEnqueue(mediaUrl);
    }

    public void requestPlatformMediaEnqueue(@NonNull PlatformMedia.Platform platform, @NonNull String platformKey) {
        if (mPlaylistChangedByUserObserver == null)
            throw new IllegalStateException("Not connected to server.");

        mPlaylistChangedByUserObserver.onUserRequestsPlatformMediaEnqueue(platform, platformKey);
    }

    public void requestPlayOf(@NonNull PlaylistEntry entry, int startSearchIndex) {
        if (mPlaylistChangedByUserObserver == null)
            throw new IllegalStateException("Not connected to server.");

        if (mIsUserAdmin)
            mPlaylistChangedByUserObserver.onUserRequestsPlayOf(entry, startSearchIndex);
    }

    /**
     * Indicates whether the user can remove the specified playlist entry.
     * A user is allowed to remove an entry if the user is admin or if the user is not admin but
     * has added the entry to the playlist.
     *
     * @param playlistPos The position of entry in the playlist
     * @return true if the user can remove the entry, false otherwise
     */
    public boolean canRemovePlaylistEntry(int playlistPos) {
        return canRemovePlaylistEntry(mPlaylist.getEntry(playlistPos));
    }
	
	public boolean canRemovePlaylistEntry(@NonNull PlaylistEntry entry) {
		return mIsUserAdmin || entry.getAdderName().equals(mUserName);
	}

    /**
     * Remove an entry from the playlist.
     *
     * The removal is not persisted until another entry is removed or {@link #commitRemove()}
     * is manually called.
     * The removal can be undone using {@link #restoreRemoved()} until it is persisted.
     *
     * @param playlistPos Position of the entry to remove in the playlist list
     * @param removedByServerCallback Callback called when the entry was removed by the server before {@link #commitRemove()} has been called
     * @return true if the entry can be removed by the user, false otherwise
     */
    public boolean requestRemove(int playlistPos, @Nullable RemovedByServerCallback removedByServerCallback) {
        if (!canRemovePlaylistEntry(playlistPos))
            return false;

        saveRemoved(mPlaylist.remove(playlistPos), playlistPos, removedByServerCallback);
        return true;
    }
	
	public boolean requestRemove(@NonNull PlaylistEntry entry, int startSearchIndex, @Nullable RemovedByServerCallback removedByServerCallback) {
		if (!canRemovePlaylistEntry(entry))
			return false;

        saveRemoved(entry, mPlaylist.remove(entry, startSearchIndex), removedByServerCallback);
		return true;
	}
	
	private void saveRemoved(@NonNull PlaylistEntry entry, int entryPos, @Nullable RemovedByServerCallback removedByServerCallback) {
		commitRemove();
		
		mRemovedEntry = new RemovedEntry(entryPos, entry, entryPos == mPlaylist.getCurrentEntryIndex());
		mRemovedByServerCallback = removedByServerCallback;
	}

    public boolean restoreRemoved() {
        if (mRemovedEntry == null)
            return false;

        mPlaylist.insert(mRemovedEntry.getPos(), mRemovedEntry.getValue());

        if (mRemovedEntry.isCurrentEntry())
            mPlaylist.setCurrentEntryIndex(mRemovedEntry.getPos());

        mRemovedEntry = null;
        mRemovedByServerCallback = null;

        return true;
    }

    // TODO : Don't forget to commit the remove when client is closed !
    public boolean commitRemove() {
        if (mPlaylistChangedByUserObserver == null)
            throw new IllegalStateException("Not connected to server.");

        if (mRemovedEntry == null)
            return false;

        mPlaylistChangedByUserObserver.onPlaylistEntryRemovedByUser(mRemovedEntry.getValue(), mRemovedEntry.getPos());

        mRemovedEntry = null;
        mRemovedByServerCallback = null;

        return true;
    }

    public void requestVoteToSkipCurrentEntry() {
        if (mPlaylistChangedByUserObserver == null)
            throw new IllegalStateException("Not connected to server.");

        if (mPlaylist.getCurrentEntryIndex() == Playlist.NO_INDEX)
            throw new IllegalStateException("The playlist has no current entry");

        voteToSkipCurrentEntry();
    }

    public boolean canVoteToSkipCurrentEntry() {
        return mIsSkipVoteEnabled && !mHasUserVoted;
    }

    public void setUserSkipVoteObserver(@Nullable UserSkipVoteObserver observer) {
        mUserSkipVoteObserver = observer;
    }

    void enqueue(@NonNull PlaylistEntry newEntry) {
        mPlaylist.enqueue(newEntry);
    }

    void remove(@NonNull PlaylistEntryReference entryInfo) {
        if (mRemovedEntry != null && entryInfo.getEntryId() == mRemovedEntry.getValue().getEntryId()) {
            mRemovedEntry = null;

            if (mRemovedByServerCallback != null) {
                mRemovedByServerCallback.onEntryRemovedByServer();
                mRemovedByServerCallback = null;
            }

            return;
        }

        mPlaylist.remove(entryInfo.getEntryId(), entryInfo.getEntryIndex());
    }

    void moveUp(@NonNull PlaylistEntryReference entryInfo) {
        mPlaylist.moveUp(entryInfo.getEntryId(), entryInfo.getEntryIndex());
    }

    void moveDown(@NonNull PlaylistEntryReference entryInfo) {
        mPlaylist.moveDown(entryInfo.getEntryId(), entryInfo.getEntryIndex());
    }

    PlaylistEntry setCurrentEntry(@NonNull PlaylistEntryReference entryInfo) {
        mHasUserVoted = false;

        if (mRemovedEntry != null) {
            if (mRemovedEntry.getId() == entryInfo.getEntryId()) {
                mRemovedEntry.setCurrentEntry(true);
                return mRemovedEntry.getValue();
            }

            mRemovedEntry.setCurrentEntry(false);
        }

        return mPlaylist.setCurrentEntry(entryInfo.getEntryId(), entryInfo.getEntryIndex());
    }

    /**
     * Returns the current playlist entry or null if there is none.
     *
     * In the case the current entry has been removed by the user
     * but the change has not been currently committed (the user may
     * therefore still rollback the delete), this method, unlike
     * {@link Playlist#getCurrentEntry()} will returns that locally
     * removed entry.
     *
     * @return The current playlist entry or null.
     */
    PlaylistEntry getCurrentEntry() {
        if (mRemovedEntry != null && mRemovedEntry.isCurrentEntry())
            return mRemovedEntry.getValue();

        return mPlaylist.getCurrentEntry();
    }

    void voteToSkipCurrentEntry() {
        if (!mIsSkipVoteEnabled)
            throw new IllegalStateException("Skip vote is disabled by the server.");

        if (mHasUserVoted)
            throw new IllegalStateException("The user has already voted.");

        mHasUserVoted = true;
        mPlaylistChangedByUserObserver.onUserVotedToSkipCurrentEntry(getCurrentEntry());

        if (mUserSkipVoteObserver != null)
            mUserSkipVoteObserver.onUserVotedToSkipCurrentEntry();
    }

    boolean hasUserVoted() {
        return mHasUserVoted;
    }

    void outOfMedia() {
        mHasUserVoted = false;
        mPlaylist.setCurrentEntryIndex(Playlist.NO_INDEX);
    }

    void setPlaylistChangedByUserObserver(@Nullable PlaylistChangedByUserObserver observer) {
        mPlaylistChangedByUserObserver = observer;
    }

    public interface RemovedByServerCallback {
        void onEntryRemovedByServer();
    }

    public interface UserSkipVoteObserver {
        void onUserVotedToSkipCurrentEntry();
    }

    interface PlaylistChangedByUserObserver {
        void onPlaylistEntryRemovedByUser(@NonNull PlaylistEntry entry, int pos);

        void onUserRequestsPlatformMediaEnqueue(@NonNull String mediaUrl);

        void onUserRequestsPlatformMediaEnqueue(@NonNull PlatformMedia.Platform platform, @NonNull String platformKey);

        void onUserRequestsPlayOf(@NonNull PlaylistEntry entry, int pos);

        void onUserVotedToSkipCurrentEntry(@NonNull PlaylistEntry currentEntry);
    }

    private static class RemovedEntry {
        private final ListItem<PlaylistEntry> mEntry;
        private boolean mIsCurrentEntry;

        RemovedEntry(int pos, PlaylistEntry value, boolean isCurrentEntry) {
            mEntry = new ListItem<>(pos, value);
            mIsCurrentEntry = isCurrentEntry;
        }

        long getId() {
            return mEntry.getValue().getEntryId();
        }

        PlaylistEntry getValue() {
            return mEntry.getValue();
        }

        int getPos() {
            return mEntry.getPos();
        }

        void setCurrentEntry(boolean isCurrentEntry) {
            mIsCurrentEntry = isCurrentEntry;
        }

        boolean isCurrentEntry() {
            return mIsCurrentEntry;
        }
    }
}
