package com.taltenbach.mediacaster.client;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StringRes;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.taltenbach.mediacaster.R;
import com.taltenbach.mediacaster.model.PlatformMedia;
import com.taltenbach.mediacaster.model.Playlist;
import com.taltenbach.mediacaster.model.PlaylistEntry;
import com.taltenbach.mediacaster.ui.ClientFragment;
import com.taltenbach.mediacaster.ui.EnqueueMediaActivity;
import com.taltenbach.mediacaster.ui.MainActivity;
import com.taltenbach.mediacaster.util.NotificationUtil;

import java.io.IOException;

public class PlaylistClientService extends Service {
    public static final String EXTRA_CONNECTION_INFO = "com.taltenbach.mediacaster.client.PlaylistClientService.CONNECTION_INFO";
    public static final String ACTION_REQUEST_ECHO = "com.taltenbach.mediacaster.client.PlaylistClientService.ACTION_REQUEST_ECHO";
    public static final String ACTION_ECHO = "com.taltenbach.mediacaster.client.PlaylistClientService.ACTION_ECHO";

    private static final String ONGOING_NOTIFICATION_CHANNEL_ID = "com.taltenbach.mediacaster.client.PlaylistClientService.ONGOING_NOTIFICATION_CHANNEL";
    private static final String DISCONNECTION_NOTIFICATION_CHANNEL_ID = "com.taltenbach.mediacaster.client.PlaylistClientService.DISCONNECTION_NOTIFICATION_CHANNEL";
    private static final int ONGOING_NOTIFICATION_ID = 42;
    private static final int DISCONNECTED_NOTIFICATION_ID = 43;

    private PlaylistClient mClient;
    private ClientClock mClientClock;
    private ClientPlaylistController mPlaylistController;
    private MediaPlayerController mPlayerController;
    private ConnectionFailedReason mConnectionFailedReason;
    private NotificationCompat.Builder mOngoingNotificationBuilder;
    private SkipVote mSkipVote;

    private ConnectionCallback mConnectionCallback;

    private BroadcastReceiver mEchoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LocalBroadcastManager
                    .getInstance(PlaylistClientService.this)
                    .sendBroadcastSync(new Intent(ACTION_ECHO));
        }
    };

    public class LocalBinder extends Binder {
        /*public PlaylistClientService getService() {
            return PlaylistClientService.this;
        }*/

        public void setConnectionCallback(ConnectionCallback callback) {
            mConnectionCallback = callback;

            if (callback == null)
                return;

            if (mPlaylistController != null) {
                callback.onConnected(mPlaylistController, mPlayerController);
            } else if (mConnectionFailedReason != null) {
                callback.onConnectionFailed(mConnectionFailedReason);
            }
        }
		
		/**
		 * Gets the playlist controller.
		 *
		 * @return The playlist controller if the service is connected, null otherwise.
		 */
		public ClientPlaylistController getPlaylistController() {
			return mPlaylistController;
		}
		
		public void kickUser(final String userName) {
			mClient.sendMessage(MessageHeader.KICK_USER, new ClientParcel() {
				@Override
				public void writeParcelTo(ClientOutputStream out) throws IOException {
					out.writeString(userName);
				}
			});
		}
		
		public void banUser(final String userName) {
			mClient.sendMessage(MessageHeader.BAN_USER, new ClientParcel() {
				@Override
				public void writeParcelTo(ClientOutputStream out) throws IOException {
					out.writeString(userName);
				}
			});
		}

        /**
         * Disconnect the service from the playlist server and terminate the service.
         * If the service is not already connected, cancel the connection and terminate the service.
         */
        public void disconnect() {
            if (mClient == null) { // onStartCommand has not been already called
                stopSelf(); // TODO: check if calling stopSelf before onStartCommand executes actually prevent the service from being executed
                return;
            }

//            if (mPlaylistController == null) { // Socket connection may have been established but the client is not already connected to the playlist server
//                Log.d("DebugClose", "Close by service");
//                mClient.close();
//                mClient = null;
//                stopSelf();
//                return;
//            }

            // The client is already connected to the playlist server
            PlaylistClientService.this.disconnect();
        }
    }

    @Override
    public void onCreate() {
        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(mEchoReceiver, new IntentFilter(ACTION_REQUEST_ECHO));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mClient != null)
            throw new IllegalStateException("Client already started.");

        ConnectionInfo connectionInfo = intent.getParcelableExtra(EXTRA_CONNECTION_INFO);

        if (connectionInfo == null)
            throw new IllegalArgumentException("The service must be started with an intent containing a ConnectionInfo extra.");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            initNotificationChannels();

        mOngoingNotificationBuilder = createOngoingNotificationBuilder(connectionInfo);

        startForeground(ONGOING_NOTIFICATION_ID, mOngoingNotificationBuilder.build());

        mClient = new PlaylistClient(getApplicationContext(), connectionInfo, mClientCallback);

        return START_REDELIVER_INTENT;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void initNotificationChannels() {
        NotificationUtil.createChannel(this,
                ONGOING_NOTIFICATION_CHANNEL_ID,
                R.string.playlist_service_ongoing_notification_channel_name,
                R.string.playlist_service_ongoing_notification_channel_desc,
                NotificationManager.IMPORTANCE_LOW);

        NotificationUtil.createChannel(this,
                DISCONNECTION_NOTIFICATION_CHANNEL_ID,
                R.string.playlist_service_disconnection_notification_channel_name,
                R.string.playlist_service_disconnection_notification_channel_desc,
                NotificationManager.IMPORTANCE_HIGH);
    }

    private PendingIntent createNotificationIntent() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        return PendingIntent.getActivity(this, 0, notificationIntent, 0);
    }

    private NotificationCompat.Builder createOngoingNotificationBuilder(ConnectionInfo connectionInfo) {
        return new NotificationCompat.Builder(this, ONGOING_NOTIFICATION_CHANNEL_ID)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_phonelink_white_24)
                .setContentTitle(getString(R.string.connecting_to, connectionInfo.getServerAddress(), connectionInfo.getServerPort()))
                .setContentIntent(createNotificationIntent());
    }

    private void updateOngoingNotification(@Nullable PlaylistEntry currentEntry) {
        CharSequence text;

        if (currentEntry == null)
            text = getText(R.string.idle);
        else
            text = getString(R.string.playing, currentEntry.getMedia().getTitle());

        mOngoingNotificationBuilder.setContentText(text);
        mOngoingNotificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(text));

        NotificationUtil.notify(this, ONGOING_NOTIFICATION_ID, mOngoingNotificationBuilder.build());
    }

    private void showDisconnectedNotification(@StringRes int title, @StringRes int message) {
        ConnectionInfo connectionInfo = mClient.getConnectionInfo();

        String text = getString(message, connectionInfo.getServerAddress(), connectionInfo.getServerPort());

        Notification notif = new NotificationCompat.Builder(this, DISCONNECTION_NOTIFICATION_CHANNEL_ID)
                                .setSmallIcon(R.drawable.ic_phonelink_off_white_24)
                                .setContentTitle(getText(title))
                                .setContentText(text)
                                .setContentIntent(createNotificationIntent())
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                                .setAutoCancel(true)
                                .build();

        NotificationUtil.notify(this, DISCONNECTED_NOTIFICATION_ID, notif);
    }

    private void refreshEnqueueIntentFilterState(boolean isConnected) {
        Context context = getApplicationContext();

        PackageManager packageManager = context.getPackageManager();
        ComponentName componentName = new ComponentName(context, EnqueueMediaActivity.class.getName());

        int state = isConnected ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED
                : PackageManager.COMPONENT_ENABLED_STATE_DISABLED;

        packageManager.setComponentEnabledSetting(componentName, state, PackageManager.DONT_KILL_APP);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new LocalBinder();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager
                .getInstance(this)
                .unregisterReceiver(mEchoReceiver);

        if (mClient != null) {
            Log.w(getClass().getSimpleName(), "Client socket has not being closed before service onDestroy");
            mClient.close(); // Ensure the client socket is closed
        }
    }

    private void disconnect() {
        if (mClient == null) // Already disconnected
            return;

        refreshEnqueueIntentFilterState(false);

        if (mPlaylistController != null) {
            mPlaylistController.commitRemove(); // TODO: Check behavior when kicked/banned and disconnected by client
            mPlaylistController.setPlaylistChangedByUserObserver(null);

            if (mSkipVote != null) {
                mSkipVote.conclude(this);
                mSkipVote = null;
            }

            if (mClientClock != null) {
                mClientClock.stop();

                mClientClock = null;
                mPlayerController = null;
            }

            mPlaylistController = null;
        }

        mClient.close();
        mClient = null;

        stopSelf();

        if (mConnectionCallback != null)
            mConnectionCallback.onDisconnected();
    }

    private final ClientCallback mClientCallback = new ClientCallback() {
        @Override
        public void onConnected(String userName, boolean isUserAdmin, boolean isSkipVoteEnabled, PlaylistEntry[] initialEntries, int currentEntryIndex,
                                MediaPlayerController.PlayerState playerState, long currentMediaTime, long mediaDuration, int remoteVolume)
        {
            if (isUserAdmin)
                Toast.makeText(getApplicationContext(), R.string.admin_rights_granted, Toast.LENGTH_SHORT).show();

            mPlaylistController = new ClientPlaylistController(userName, isUserAdmin, isSkipVoteEnabled, initialEntries, currentEntryIndex);
            mPlaylistController.setPlaylistChangedByUserObserver(mPlaylistChangedByUserObserver);

            if (isUserAdmin) {
                mClientClock = new ClientClock(mClient);
                mPlayerController = new MediaPlayerController(mClient, mPlaylistController.getPlaylist(), mClientClock);
                mPlayerController.notifyPlayerStateChanged(playerState, currentMediaTime, mediaDuration);
                mPlayerController.notifyRemoteVolumeChanged(remoteVolume);
            }

            ConnectionInfo connectionInfo = mClient.getConnectionInfo();
            mOngoingNotificationBuilder.setContentTitle(getString(R.string.connected_to, connectionInfo.getServerAddress(), connectionInfo.getServerPort()));

            updateOngoingNotification(currentEntryIndex != Playlist.NO_INDEX ? initialEntries[currentEntryIndex] : null);

            refreshEnqueueIntentFilterState(true);

            if (mConnectionCallback != null)
                mConnectionCallback.onConnected(mPlaylistController, mPlayerController);
        }

        @Override
        public void onConnectionFailed(ConnectionFailedReason reason) {
            stopSelf();

            mClient = null;

            mConnectionFailedReason = reason;

            if (mConnectionCallback != null)
                mConnectionCallback.onConnectionFailed(reason);
        }

        @Override
        public void onKicked() {
            /*if (mConnectionCallback != null)
                mConnectionCallback.onKicked();*/

            showDisconnectedNotification(R.string.on_kicked_title, R.string.on_kicked_desc);

            disconnect();
        }

        @Override
        public void onBanned() {
            /*if (mConnectionCallback != null)
                mConnectionCallback.onBanned();*/

            showDisconnectedNotification(R.string.on_banned_title, R.string.on_banned_desc);

            disconnect();
        }

        @Override
        public void onServerStopped() {
            /*if (mConnectionCallback != null)
                mConnectionCallback.onServerStopped();*/

            showDisconnectedNotification(R.string.on_server_stopped_title, R.string.on_server_stopped_desc);

            disconnect();
        }

        @Override
        public void onConnectionLost() {
            /*if (mConnectionCallback != null)
                mConnectionCallback.onConnectionLost();*/

            showDisconnectedNotification(R.string.on_connection_lost_title, R.string.on_connection_lost_desc);

            disconnect();
        }

        @Override
        public void onEntryEnqueued(PlaylistEntry newEntry) {
            mPlaylistController.enqueue(newEntry);
        }

        @Override
        public void onEntryRemoved(PlaylistEntryReference entryRef) {
            mPlaylistController.remove(entryRef);
        }

        @Override
        public void onEntryMovedUp(PlaylistEntryReference entryRef) {
            mPlaylistController.moveUp(entryRef);
        }

        @Override
        public void onEntryMovedDown(PlaylistEntryReference entryRef) {
            mPlaylistController.moveDown(entryRef);
        }

        @Override
        public void onCurrentEntryChanged(PlaylistEntryReference newCurrentEntryRef) {
            PlaylistEntry newCurrentEntry = mPlaylistController.setCurrentEntry(newCurrentEntryRef);

            updateOngoingNotification(newCurrentEntry);

            if (mSkipVote != null) {
                mSkipVote.conclude(PlaylistClientService.this);
                mSkipVote = null;
            }
        }

        @Override
        public void onOutOfMedia() {
            mPlaylistController.outOfMedia();

            updateOngoingNotification(null);

            if (mSkipVote != null) {
                mSkipVote.conclude(PlaylistClientService.this);
                mSkipVote = null;
            }
        }

        @Override
        public void onEnqueueFailed(String errorMsg) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.platform_media_enqueue_failed, errorMsg),
                    Toast.LENGTH_LONG)
                .show();
        }

        @Override
        public void onSkipVoteUpdated(int favorableVotes, int favorableVotesNeeded) {
            if (mSkipVote == null) {
                mSkipVote = new SkipVote(PlaylistClientService.this, favorableVotes, favorableVotesNeeded, new SkipVote.UserVoteCallback() {
                    @Override
                    public void onUserVotedYes() {
                        if (mPlaylistController.getCurrentEntry() == null)
                            return; // The entry has been irremediably removed by user, and will thus be removed soon on the server

                        mPlaylistController.voteToSkipCurrentEntry();
                    }
                });

                if (mPlaylistController.hasUserVoted())
                    mSkipVote.disableUserVote(PlaylistClientService.this);
            } else {
                mSkipVote.update(PlaylistClientService.this, favorableVotes, favorableVotesNeeded);
            }
        }

        @Override
        public void onPongReceived() {
            if (mClientClock != null)
                mClientClock.notifyPongReceived();
        }

        @Override
        public void onPlayerStateChanged(MediaPlayerController.PlayerState playerState, long currentMediaTime, long mediaDuration) {
            if (mPlayerController != null)
                mPlayerController.notifyPlayerStateChanged(playerState, currentMediaTime, mediaDuration);
        }

        @Override
        public void onRemoteVolumeChanged(int volume) {
            if (mPlayerController != null)
                mPlayerController.notifyRemoteVolumeChanged(volume);
        }
    };

    private final ClientPlaylistController.PlaylistChangedByUserObserver mPlaylistChangedByUserObserver = new ClientPlaylistController.PlaylistChangedByUserObserver() {
        @Override
        public void onPlaylistEntryRemovedByUser(@NonNull PlaylistEntry entry, int pos) {
            mClient.sendMessage(MessageHeader.REMOVE_ENTRY, new PlaylistEntryReference(entry.getEntryId(), pos));
        }

        @Override
        public void onUserRequestsPlatformMediaEnqueue(@NonNull final String mediaUrl) {
            mClient.sendMessage(MessageHeader.ENQUEUE_MEDIA_URL, new ClientParcel() {
                @Override
                public void writeParcelTo(ClientOutputStream out) throws IOException {
                    out.writeString(mediaUrl);
                }
            });
        }

        @Override
        public void onUserRequestsPlatformMediaEnqueue(@NonNull final PlatformMedia.Platform platform, @NonNull final String platformKey) {
            mClient.sendMessage(MessageHeader.ENQUEUE_MEDIA_ID, new ClientParcel() {
                @Override
                public void writeParcelTo(ClientOutputStream out) throws IOException {
                    out.writeShort(platform.getId());
                    out.writeString(platformKey);
                }
            });
        }

        @Override
        public void onUserRequestsPlayOf(@NonNull PlaylistEntry entry, int pos) {
            mClient.sendMessage(MessageHeader.PLAY_ENTRY, new PlaylistEntryReference(entry.getEntryId(), pos));
        }

        @Override
        public void onUserVotedToSkipCurrentEntry(@NonNull final PlaylistEntry currentEntry) {
            if (mSkipVote != null)
                mSkipVote.disableUserVote(PlaylistClientService.this);

            mClient.sendMessage(MessageHeader.VOTE_TO_SKIP_CURRENT, new ClientParcel() {
                @Override
                public void writeParcelTo(ClientOutputStream out) throws IOException {
                    out.writeLong(currentEntry.getEntryId());
                }
            });
        }
    };

    public interface ConnectionCallback {
        void onConnected(@NonNull ClientPlaylistController playlistController, @Nullable MediaPlayerController playerController);

        void onConnectionFailed(ConnectionFailedReason reason);

        /*void onKicked();

        void onBanned();

        void onServerStopped();

        void onConnectionLost();*/

        void onDisconnected();
    }
}
