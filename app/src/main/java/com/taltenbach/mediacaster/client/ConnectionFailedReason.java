package com.taltenbach.mediacaster.client;

import android.support.annotation.StringRes;
import android.util.SparseArray;

import com.taltenbach.mediacaster.R;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.client
 * Creator : Thomas ALTENBACH
 * Date : 08/07/2018
 */

public enum ConnectionFailedReason {
    SOCKET_ERROR(-1, R.string.server_not_found),
    INCOMPATIBLE_PROTOCOL(1, R.string.incompatible_protocol),
    USER_BANNED(2, R.string.banned_from_server),
    DUPLICATE_USER_NAME(3, R.string.duplicate_user_name),
    DUPLICATE_USER_ID(4, R.string.duplicate_user_id);

    private static final SparseArray<ConnectionFailedReason> VALUES = new SparseArray<>();

    static {
        for (ConnectionFailedReason reason : ConnectionFailedReason.values())
            VALUES.append(reason.mId, reason);
    }

    private final int mMessageResId;
    private final short mId;

    ConnectionFailedReason(int id, @StringRes int messageResId) {
        mId = (short) id;
        mMessageResId = messageResId;
    }

    short getId() {
        return mId;
    }

    @StringRes
    public int getMessageResId() {
        return mMessageResId;
    }

    static ConnectionFailedReason fromId(short id) {
        return VALUES.get(id);
    }
}
