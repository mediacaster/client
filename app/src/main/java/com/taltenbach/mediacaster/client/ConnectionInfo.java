package com.taltenbach.mediacaster.client;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

// TODO: put user name apart ?
public final class ConnectionInfo implements Parcelable {
    private final int mServerPort;
    private final String mServerAddress;
    private final String mUserName;
    private final String mAdminPassword;

    public ConnectionInfo(@NonNull String serverAddress, int serverPort, @NonNull String userName, @NonNull String adminPassword) {
        mServerAddress = serverAddress;
        mServerPort = serverPort;
        mUserName = userName;
        mAdminPassword = adminPassword;
    }

    private ConnectionInfo(Parcel in) {
        mServerPort = in.readInt();
        mServerAddress = in.readString();
        mUserName = in.readString();
        mAdminPassword = in.readString();
    }

    public static final Creator<ConnectionInfo> CREATOR = new Creator<ConnectionInfo>() {
        @Override
        public ConnectionInfo createFromParcel(Parcel in) {
            return new ConnectionInfo(in);
        }

        @Override
        public ConnectionInfo[] newArray(int size) {
            return new ConnectionInfo[size];
        }
    };

    public String getServerAddress() {
        return mServerAddress;
    }

    public int getServerPort() {
        return mServerPort;
    }

    public String getUserName() {
        return mUserName;
    }

    public String getAdminPassword() {
        return mAdminPassword;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mServerPort);
        dest.writeString(mServerAddress);
        dest.writeString(mUserName);
        dest.writeString(mAdminPassword);
    }
}
