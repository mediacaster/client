package com.taltenbach.mediacaster.client;

import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.client
 * Creator : Thomas ALTENBACH
 * Date : 20/08/2018
 */

final class MediaTimer {
    private static final int UPDATE_INTERVAL = 1000;

    private final ClientClock mClientClock;
    private final Handler mHandler;
    private final UpdateCallback mUpdateCallback;

    private long mLastUpdateTime;
    private long mCurrentMediaTime;
    private long mDuration;
    private boolean mIsCallbackEnabled;

    private final Runnable mUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            update();
        }
    };

    MediaTimer(@NonNull ClientClock clientClock, @NonNull UpdateCallback updateCallback) {
        mClientClock = clientClock;
        mHandler = new Handler();
        mUpdateCallback = updateCallback;
        mIsCallbackEnabled = true;
        mDuration = -1;
        mLastUpdateTime = -1;
    }

    /**
     * Starts or restarts the timer from 0 ms to the given duration.
     * @param mediaDuration Duration of the media (in seconds)
     */
    void start(long mediaDuration) {
        startAt(0, mediaDuration);
    }

    /**
     * Starts or restarts the timer from the specified media time to the given duration.
     * @param serverCurrentMediaTime Current media time on the server (in ms)
     * @param mediaDuration Duration of the media (in ms)
     */
    void startAt(long serverCurrentMediaTime, long mediaDuration) {
        if (isRunning())
            throw new IllegalStateException("Timer is already running");

        if (mediaDuration < 0)
            throw new IllegalStateException("Duration must be positive or zero.");

        if (serverCurrentMediaTime < 0 || serverCurrentMediaTime > mediaDuration)
            throw new IllegalArgumentException("Start media time must be positive or zero and less than or equal to media duration.");

        mDuration = mediaDuration;
        mCurrentMediaTime = mClientClock.getCurrentTime(serverCurrentMediaTime); // Take into account the latency between client and server (time it takes for the start or resume message to be received)
        mLastUpdateTime = SystemClock.elapsedRealtime();

        if (mIsCallbackEnabled)
            update();
    }

    /**
     * Pauses the media timer at the specified media time.
     * If the timer is not running, this method starts the timer in paused state.
     *
     * @param pausedTime The media time at which the media is paused (in ms)
     */
    void pauseAt(long pausedTime) {
        boolean isRunning = isRunning();

        if (pausedTime < 0 || (isRunning && pausedTime > mDuration))
            throw new IllegalArgumentException("Paused time must be positive or zero and less than or equal to media duration.");

        if (isRunning && mIsCallbackEnabled)
            mHandler.removeCallbacks(mUpdateRunnable);

        mCurrentMediaTime = pausedTime;
        mDuration = -1;
        mLastUpdateTime = 0; // Put a value different from -1 to set the timer in started state in the case it is not currently started

        if (mIsCallbackEnabled)
            mUpdateCallback.onUpdate(pausedTime);
    }

    void stop() {
        if (isRunning() && mIsCallbackEnabled)
            mHandler.removeCallbacks(mUpdateRunnable);

        mDuration = -1;
        mLastUpdateTime = -1;
    }

    /**
     * Indicates if this timer is running (ie. neither paused nor stopped).
     * @return true if the timer is running, false otherwise
     */
    boolean isRunning() {
        return mDuration != -1;
    }

    /**
     * Indicates if this timer has been started (ie. is running or paused)
     * @return true if the timer is started, false otherwise
     */
    boolean isStarted() {
        return mLastUpdateTime != -1;
    }

    /**
     * If the timer is running, indicates the current media time.
     * If the timer is paused, indicates the media time at which the timer has been paused.
     * This function cannot be called if the timer is stopped.
     * @return The current media time (in ms)
     */
    long getCurrentMediaTime() {
        if (isRunning())
            return Math.min(mCurrentMediaTime + (SystemClock.elapsedRealtime() - mLastUpdateTime), mDuration);
        else if (isStarted())
            return mCurrentMediaTime;
        else
            throw new IllegalStateException("The timer is stopped.");
    }

    void setCallbackEnabled(boolean isCallbackEnabled) {
        if (mIsCallbackEnabled == isCallbackEnabled)
            return;

        mIsCallbackEnabled = isCallbackEnabled;

        if (!isRunning())
            return;

        if (isCallbackEnabled) {
            updateCurrentMediaTime();

            if (mCurrentMediaTime > mDuration) {
                mCurrentMediaTime = mDuration;
                mDuration = -1;
                return;
            }

            if (mCurrentMediaTime % UPDATE_INTERVAL == 0)
                mUpdateCallback.onUpdate(mCurrentMediaTime);

            scheduleNextUpdate();
        } else {
            mHandler.removeCallbacks(mUpdateRunnable);
        }
    }

    private void update() {
        updateCurrentMediaTime();

        if (mCurrentMediaTime >= mDuration) {
            mUpdateCallback.onUpdate(mDuration);
            mCurrentMediaTime = mDuration;
            mDuration = -1;
            return;
        }

        mUpdateCallback.onUpdate(mCurrentMediaTime);

        scheduleNextUpdate();
    }

    private void updateCurrentMediaTime() {
        long currentTime = SystemClock.elapsedRealtime();
        mCurrentMediaTime += currentTime - mLastUpdateTime;
        mLastUpdateTime = currentTime;
    }

    private void scheduleNextUpdate() {
        long currentMediaTime = mCurrentMediaTime + (SystemClock.elapsedRealtime() - mLastUpdateTime);
        long millisLeft = mDuration - currentMediaTime;
        long nextTickDelay;

        if (millisLeft < UPDATE_INTERVAL)
            nextTickDelay = Math.max(millisLeft, 0);
        else
            nextTickDelay = UPDATE_INTERVAL - (currentMediaTime % UPDATE_INTERVAL);

        mHandler.postDelayed(mUpdateRunnable, nextTickDelay);
    }

    interface UpdateCallback {
        void onUpdate(long currentTime);
    }
}
