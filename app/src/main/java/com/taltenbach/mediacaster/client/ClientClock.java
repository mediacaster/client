package com.taltenbach.mediacaster.client;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Arrays;

/**
 * Project : MediaCaster
 * Package : com.taltenbach.mediacaster.client
 * Creator : Thomas ALTENBACH
 * Date : 20/08/2018
 */

final class ClientClock {
    private static final int RESYNC_MSG_INTERVAL = 30000;

    private final Handler mMainHandler;
    private final PlaylistClient mClient;

    private long mLastPingSentTime;
    private float mAverageLatency;                     // Average latency between the client and server, defined by the half of the RTT
    private InitialSyncTask mInitialSyncTask;

    private ResyncObserver mResyncObserver;

    private final Runnable mSendPingRunnable = new Runnable() {
        @Override
        public void run() {
            sendPing();
        }
    };

    ClientClock(@NonNull PlaylistClient client) {
        mMainHandler = new Handler(Looper.getMainLooper());
        mClient = client;
        mInitialSyncTask = new InitialSyncTask();

        mInitialSyncTask.start();
    }

    private void sendPing() {
        mClient.sendMessage(MessageHeader.PING);
        mLastPingSentTime = SystemClock.elapsedRealtime();
    }

    private void setAverageLatency(float averageLatency) {
        float oldLatency = mAverageLatency;
        mAverageLatency = averageLatency;

        if (mResyncObserver != null)
            mResyncObserver.onResync((int) (averageLatency - oldLatency));
    }

    long getCurrentTime(long serverTime) {
        return (long) (serverTime + mAverageLatency);
    }

    void notifyPongReceived() {
        if (mInitialSyncTask != null) {
            mInitialSyncTask.notifyPongReceived();
            return;
        }

        float latency = (SystemClock.elapsedRealtime() - mLastPingSentTime) / 2.0f;

        setAverageLatency(0.875f * mAverageLatency + 0.125f * latency);

        mMainHandler.postDelayed(mSendPingRunnable, RESYNC_MSG_INTERVAL);
    }

    void stop() {
        mMainHandler.removeCallbacks(mSendPingRunnable);
        mResyncObserver = null;
    }

    void setResyncObserver(@Nullable ResyncObserver observer) {
        mResyncObserver = observer;
    }

    interface ResyncObserver {
        /**
         * Notifies that the clock was resynchronized with the server clock
         * @param timeDiff The difference the new time and the old one
         */
        void onResync(int timeDiff);
    }

    private final class InitialSyncTask {
        private static final int INITIAL_SYNC_MSG_COUNT = 15;
        private static final int INITIAL_SYNC_MSG_INTERVAL = 2000;

        private final int[] mRtts;
        private int mSyncMsgCount;

        InitialSyncTask() {
            mRtts = new int[INITIAL_SYNC_MSG_COUNT];
        }

        void start() {
            sendPing();
        }

        void notifyPongReceived() {
            mRtts[mSyncMsgCount++] = (int) (SystemClock.elapsedRealtime() - mLastPingSentTime);

            if (mSyncMsgCount == INITIAL_SYNC_MSG_COUNT) {
                computeAverageLatency();
                mMainHandler.postDelayed(mSendPingRunnable, RESYNC_MSG_INTERVAL);
                mInitialSyncTask = null;
            } else {
                if (mSyncMsgCount == 1)
                    setAverageLatency(mRtts[0]); // Set a first approximation

                mMainHandler.postDelayed(mSendPingRunnable, INITIAL_SYNC_MSG_INTERVAL);
            }
        }

        void computeAverageLatency() {
            Arrays.sort(mRtts);

            int sum = 0;
            int squareSum = 0;

            for (int rtt : mRtts) {
                sum += rtt;
                squareSum += rtt * rtt;
            }

            double average = (double) sum / INITIAL_SYNC_MSG_COUNT;
            double variance = (squareSum - INITIAL_SYNC_MSG_COUNT * average * average) / (INITIAL_SYNC_MSG_COUNT - 1);
            double stdDev = Math.sqrt(variance);

            int median = mRtts[INITIAL_SYNC_MSG_COUNT / 2];
            int correctedSum = 0;
            int validMeasurementCount = 0;

            for (int rtt : mRtts) {
                if (Math.abs(rtt - median) > stdDev)
                    continue;

                correctedSum += rtt;
                ++validMeasurementCount;
            }

            setAverageLatency(correctedSum / (2.0f * validMeasurementCount));
        }
    }
}
