package com.taltenbach.mediacaster.client;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Manage the io operations between the client and the playlist server.
 * The client is automatically closed when the connection is terminated by the server (following a
 * server stop or a client kick/ban) or when the internet connection is lost, and can be manually
 * closed by the user using {@link #close()}. In the latter case no client callback method is called.
 *
 * This class is *not* thread safe and all methods *must* be called in the main android thread.
 */
class PlaylistClient {
    private static final short CLIENT_PROTOCOL_VERSION = 1;

    private final ConnectionInfo mConnectionInfo;
    private final String mDeviceId;
    private final ClientCallback mCallback;
    private final Handler mMainHandler;

    private ConnectionTask mConnectionTask;
    private Socket mSocket;
    private ClientOutputStream mOut;
    private Handler mOutHandler;
    private Thread mListenerThread;

    @SuppressLint("HardwareIds")
    PlaylistClient(@NonNull Context context, @NonNull ConnectionInfo connectionInfo, @NonNull ClientCallback callback) {
        mConnectionInfo = connectionInfo;
        mDeviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        mCallback = callback;
        mMainHandler = new Handler(Looper.getMainLooper());

        mConnectionTask = new ConnectionTask(this);
        mConnectionTask.execute(connectionInfo);
    }

    ConnectionInfo getConnectionInfo() {
        return mConnectionInfo;
    }

    void sendMessage(final MessageHeader header, final ClientParcel... contents) {
        if (mSocket == null)
            throw new IllegalStateException("Client has been closed.");

        mOutHandler.post(new Runnable() {
            @Override
            public void run() {
                ClientOutputStream out = mOut;

                if (out == null)
                    return;

                try {
                    out.writeShort(header.getId());

                    for (ClientParcel parcel : contents)
                        parcel.writeParcelTo(out);
                } catch (final IOException e) {
                    mMainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.i("ClientOutputThread", "Connection lost.", e);
                            close();
                            mCallback.onConnectionLost();
                        }
                    });
                }
            }
        });
    }

    /**
     * Closes the connection between the client and the playlist server.
     * Does nothing if the connection is already closed.
     */
    void close() {
        if (mConnectionTask != null) {
            mConnectionTask.cancel(true);
            mConnectionTask = null;
            return;
        }

        if (mSocket == null)
            return;

        Log.d("CloseDebug", "Closing client, mListenerThread == null is " + (mListenerThread == null));

        // Interrupts the listener thread before closing the socket to avoid calling onConnectionLost() callback
        // in the case this method was manually called by the user
        if (mListenerThread != null) {
            mListenerThread.interrupt();
            Log.d("CloseDebug", "Listener thread interrupted !");
        }

        mOutHandler.getLooper().quit();

        try {
            mSocket.close();
            Log.d("CloseDebug", "Socket closed.");
        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), "Error while closing socket.", e);
        }

        mSocket = null;
        mListenerThread = null;
        mOut = null;
        mOutHandler = null;
    }

    private void onConnectionSucceed(@NonNull Socket socket) {
        mSocket = socket;
        mConnectionTask = null;

        try {
            mListenerThread = new Thread(new ServerListenerRunnable(this, socket.getInputStream(), mCallback), "ServerListenerThread");
            mOut = new ClientOutputStream(socket.getOutputStream());

            HandlerThread outThread = new HandlerThread("ClientOutputThread");
            outThread.start();

            mOutHandler = new Handler(outThread.getLooper());

            mListenerThread.start();

            ClientInfo clientInfo = new ClientInfo(CLIENT_PROTOCOL_VERSION, mConnectionInfo.getUserName(), mDeviceId, mConnectionInfo.getAdminPassword());
            sendMessage(MessageHeader.CLIENT_INFO, clientInfo);
        } catch (IOException e) {
            mListenerThread = null;
            mOut = null;

            Log.e(getClass().getSimpleName(), "Error while initializing connection.", e);
            onConnectionFailed();
        }
    }

    private void onConnectionFailed() {
        close();
        mCallback.onConnectionFailed(ConnectionFailedReason.SOCKET_ERROR);
    }

    private static class ConnectionTask extends AsyncTask<ConnectionInfo, Void, Socket> {
        private static final int CONNECTION_TIMEOUT = 5000;

        private final WeakReference<PlaylistClient> mClient;

        ConnectionTask(PlaylistClient client) {
            mClient = new WeakReference<>(client);
        }

        @Override
        protected Socket doInBackground(ConnectionInfo... connectionInfoList) {
            if (connectionInfoList.length == 0)
                throw new IllegalArgumentException("No connection info has been provided.");

            ConnectionInfo connectionInfo = connectionInfoList[0];

            Socket socket = new Socket();

            try {
                // TODO : use SSL for credentials transmission
                socket.connect(new InetSocketAddress(connectionInfo.getServerAddress(), connectionInfo.getServerPort()), CONNECTION_TIMEOUT);
            } catch (IOException e) {
                Log.i(getClass().getSimpleName(),
                        String.format("Unable to connect to server %s:%d", connectionInfo.getServerAddress(), connectionInfo.getServerPort()),
                        e);
            }

            return socket;
        }

        @Override
        protected void onPostExecute(Socket socket) {
            PlaylistClient client = mClient.get();

            if (client == null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    Log.e(getClass().getSimpleName(), "Error while closing socket", e);
                }

                return;
            }

            if (socket.isConnected())
                client.onConnectionSucceed(socket);
            else
                client.onConnectionFailed();
        }

        @Override
        protected void onCancelled(Socket result) {
            if (result == null)
                return;

            try {
                result.close();
            } catch (IOException e) {
                Log.e(getClass().getSimpleName(), "Error while closing socket", e);
            }
        }
    }
}
