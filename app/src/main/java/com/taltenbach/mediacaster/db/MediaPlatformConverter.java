package com.taltenbach.mediacaster.db;

import android.arch.persistence.room.TypeConverter;

import com.taltenbach.mediacaster.model.PlatformMedia;

final class MediaPlatformConverter {
    private MediaPlatformConverter() {
        // Empty
    }

    @TypeConverter
    public static short toShort(PlatformMedia.Platform platform) {
        return platform.getId();
    }

    @TypeConverter
    public static PlatformMedia.Platform toMediaPlatform(short platformOrdinal) {
        return PlatformMedia.Platform.fromId(platformOrdinal);
    }
}
