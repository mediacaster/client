package com.taltenbach.mediacaster.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import com.taltenbach.mediacaster.model.PlatformMedia;

import java.util.List;

@TypeConverters(MediaPlatformConverter.class)
@Dao
public interface FavoriteDao {
    @Query("SELECT * FROM favorite")
    List<FavoriteEntity> getAll();

    @Query("SELECT * FROM favorite WHERE platform = :platform AND platformKey = :platformKey LIMIT 1")
    FavoriteEntity getById(PlatformMedia.Platform platform, String platformKey);

    @Insert
    void insert(FavoriteEntity favorite);

    @Update
    void update(FavoriteEntity favorite);

    @Query("DELETE FROM favorite WHERE platform = :platform AND platformKey = :platformKey")
    void deleteById(PlatformMedia.Platform platform, String platformKey);
}
