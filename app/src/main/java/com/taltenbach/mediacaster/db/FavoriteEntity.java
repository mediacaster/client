package com.taltenbach.mediacaster.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.taltenbach.mediacaster.model.Media;
import com.taltenbach.mediacaster.model.PlatformMedia;

@Entity(primaryKeys = {"platform", "platformKey"}, tableName = "favorite")
public class FavoriteEntity {
    @ColumnInfo(name = "platform")
    @TypeConverters(MediaPlatformConverter.class)
    @NonNull
    private final PlatformMedia.Platform mPlatform;

    @ColumnInfo(name = "platformKey")
    @NonNull
    private final String mPlatformKey;

    @ColumnInfo(name = "title")
    private final String mTitle;

    @ColumnInfo(name = "mediaType")
    @TypeConverters(MediaTypeConverter.class)
    private final Media.MediaType mMediaType;

    @ColumnInfo(name = "thumbnailUrl")
    private final String mThumbnailUrl;

    public FavoriteEntity(@NonNull PlatformMedia.Platform platform, @NonNull String title, @NonNull String platformKey, @NonNull Media.MediaType mediaType, String thumbnailUrl) {
        mPlatform = platform;
        mPlatformKey = platformKey;
        mTitle = title;
        mMediaType = mediaType;
        mThumbnailUrl = thumbnailUrl;
    }

    public FavoriteEntity(PlatformMedia platformMedia) {
        mPlatform = platformMedia.getPlatform();
        mPlatformKey = platformMedia.getPlatformKey();
        mTitle = platformMedia.getTitle();
        mMediaType = platformMedia.getMediaType();
        mThumbnailUrl = platformMedia.getThumbnailUrl();
    }

    public PlatformMedia.Platform getPlatform() {
        return mPlatform;
    }

    public String getPlatformKey() {
        return mPlatformKey;
    }

    public String getTitle() {
        return mTitle;
    }

    public Media.MediaType getMediaType() {
        return mMediaType;
    }

    public String getThumbnailUrl() {
        return mThumbnailUrl;
    }
}
