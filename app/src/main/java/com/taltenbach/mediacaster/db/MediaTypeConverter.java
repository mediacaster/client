package com.taltenbach.mediacaster.db;

import android.arch.persistence.room.TypeConverter;

import com.taltenbach.mediacaster.model.Media;

final class MediaTypeConverter {
    private MediaTypeConverter() {
        // Empty
    }

    @TypeConverter
    public static int toInt(Media.MediaType type) {
        return type.ordinal();
    }

    @TypeConverter
    public static Media.MediaType toMediaType(int typeOrdinal) {
        return Media.MediaType.fromOrdinal(typeOrdinal);
    }
}
