package com.taltenbach.mediacaster.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.taltenbach.mediacaster.model.PlatformMedia;

@Database(entities = {FavoriteEntity.class}, version = 1)
public abstract class FavoriteDatabase extends RoomDatabase {
    private static FavoriteDatabase sInstance;

    public abstract FavoriteDao favoriteDao();

    public static FavoriteDatabase getInstance(Context context) {
        if (sInstance == null) {
            sInstance = Room.databaseBuilder(context.getApplicationContext(),
                    FavoriteDatabase.class, "favoriteDatabase")
                    .build();
        }

        return sInstance;
    }
}
