# MediaCaster - Client
MediaCaster enables users with an Android device to add audio-visual content such as YouTube videos to a shared playlist that is played by a server.

This repository contains the client application, the desktop and embedded server applications can respectively be found on the [Server](https://gitlab.com/mediacaster/server) and [µServer](https://gitlab.com/mediacaster/microserver) repositories.

This project has been developed as a personal project in 2018. I wanted in particular through this project to improve my Android and C++/Qt programming skills.

![Imgur Image](https://i.imgur.com/ks6ftmy.jpg)

## Technologies
* Java
* Android
* Room Persistence Library

## Getting started
### Prerequisites
* Android 4.1 or above.

### Installation
The application APK can be downloaded on your Android device from the [Releases](https://gitlab.com/mediacaster/client/-/releases) page. Simply run the file to install it on the device.

## Gallery
<img src="https://i.imgur.com/ks6ftmy.jpg" width="24.5%"/>
<img src="https://i.imgur.com/budE7tE.jpg" width="24.5%"/>
<img src="https://i.imgur.com/9hopwhR.jpg" width="24.5%"/>
<img src="https://i.imgur.com/FcHl5MY.jpg" width="24.5%"/>

## Authors
* ALTENBACH Thomas - @taltenba
